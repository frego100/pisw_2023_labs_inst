<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\OperadorInstituto;
use Illuminate\Http\Request;

class AsignarOperadorInstitutoController extends Controller
{
    // Listar los operadores de institutos
    public function listarOperadoresInstituto($idinstituto) {

      $idinstituto = (int) $idinstituto;
      //Verificar que el idlaboratorio es de tipo integer
      if($idinstituto === 0) {
          return response()->json(['message' => 'Tipo de dato no válido']);
      }

      $operadores = OperadorInstituto::where('instituto_id', $idinstituto)->orderBy('asignar_id', 'asc')
                                  ->with('operador')
                                  ->get();

      if(count($operadores) == 0) {
        return response()->json(['message' => 'El instituto no tiene operadores asignados' ], 404);
      }

      return response()->json(['operadores'=>$operadores], 200);

    }

    public function asignarOperadorInstituto(Request $request, $idinstituto) {
      $idinstituto = (int) $idinstituto;
      //Verificar que el idlaboratorio es de tipo integer
      if($idinstituto === 0) {
          return response()->json(['message' => 'Tipo de dato no válido']);
      }

      $request->validate([
        'operadores' => 'required',
      ]);

      foreach ($request->operadores as $operador) {
        $asignarOperador = new OperadorInstituto();
        $asignarOperador->operador_id = $operador;
        $asignarOperador->instituto_id = $idinstituto;
        $asignarOperador->save();
      }
      return response()->json(['message'=>'Operadores asignados correctamente']);
    }
}
