<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Equipo;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class EquipoController extends Controller
{
    //

    public function index($idregistrarlaboratio)
    {
        $equipos = Equipo::where('registro_id','=',$idregistrarlaboratio)->orderBy('equipo_id', 'asc')->get();
        return response()->json(['equipos' => $equipos]);
    }

    public function store(Request $request)
    {
        try {
            $equipo = new Equipo;
            $equipo->registro_id = $request->registro_id;
            $equipo->nombre_imagen = $request->nombre_imagen;
            $equipo->nombre = $request->nombre;
            $equipo->marca_modelo = $request->marca_modelo;
            $equipo->proveedor = $request->proveedor;
            $equipo->contacto_proveedor = $request->contacto_proveedor;
            $equipo->fecha_adquisicion = $request->fecha_adquisicion;
            $equipo->codigo_patrimonio = $request->codigo_patrimonio;
            $equipo->accesorio = $request->accesorio;
            $equipo->insumos = $request->insumos;
            $equipo->descripcion = $request->descripcion;
            
            // Manejar la imagen
            if ($request->hasFile('imagen_equipo')) {
              $path = $request->file('imagen_equipo')->store('public/imagenes/equipos');
              $equipo->imagen_equipo = substr($path, 7);

              // $imagen = $request->file('imagen_equipo');
              // $imagenNombre = $imagen->getClientOriginalName();
              // $imagenBinaria = file_get_contents($imagen->getRealPath());
              // $proyecto->imagen_equipo = $imagenBinaria;
              //$proyecto->imagen_equipos_nombre = $imagenNombre;
            }
            
            $equipo->save();

            return response()->json(['status' => 'Creado Correctamente']); 
        } catch (QueryException $e) {
          return response()->json(['message'=> 'Ocurrio un error', 'error'=> $e]);
        }
    }


    /**
     * Mostrar equipo por id
     *
     * @param  int $idproyecto
     * @return \Illuminate\Http\Response
     */
    public function show($idequipo)
    {
        $idequipo = (int) $idequipo;
        //Verificar que el idequipo es de tipo integer
        if($idequipo === 0) {
            return response()->json(['message' => 'Tipo de dato no válido']);
        }

        $equipo = Equipo::where('equipo_id','=',$idequipo)->first();
        if($equipo){
            return response()->json(['equipo' => $equipo]);
        } else {
            return response()->json(['message' => 'Equipo no encontrada'], 404);
        }
    }

    /**
     * Actualizar equipo  por id
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int $idproyecto
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $idequipo)
    {
        $idequipo = (int) $idequipo;
        //Verificar que el idproyecto es de tipo integer
        if($idequipo === 0) {
            return response()->json(['message' => 'Tipo de dato no válido']);
        }
        $equipo = Equipo::where('equipo_id','=',$idequipo)->first();
        if($equipo){
            try{
                // $equipo->registro_id = $request->registro_id;
                $equipo->nombre_imagen = $request->nombre_imagen;
                $equipo->nombre = $request->nombre;
                $equipo->marca_modelo = $request->marca_modelo;
                $equipo->fecha_adquisicion = $request->fecha_adquisicion;
                $equipo->proveedor = $request->proveedor;
                $equipo->contacto_proveedor = $request->contacto_proveedor;
                $equipo->codigo_patrimonio = $request->codigo_patrimonio;
                $equipo->accesorio = $request->accesorio;
                $equipo->insumos = $request->insumos;
                $equipo->descripcion = $request->descripcion;
                
                // Manejar la imagen
                if ($request->hasFile('imagen_equipo')) {
                  Storage::delete($equipo->imagen_equipo); // Eliminamos el archivo antiguo
                  $path = $request->file('imagen_equipo')->store('public/imagenes/equipos');
                  $equipo->imagen_equipo = substr($path, 7);

                    // $imagen = $request->file('imagen_equipo');
                    // $imagenNombre = $imagen->getClientOriginalName();
                    // $imagenBinaria = file_get_contents($imagen->getRealPath());
                    // $equipo->imagen_equipo = $imagenBinaria;
                    // $proyecto->imagen_equipos_nombre = $imagenNombre;
                }
                $equipo->update();
                return response()->json(['status' => 'Actualizado Correctamente']); 
            } catch (QueryException $e){
              return response()->json(['message'=> 'Ocurrio un error', 'error'=> $e]);
            }
        } else {
            return response()->json(['message' => 'Proyecto no encontrada'], 404);
        }
    }

    /**
     * Eliminar proyecto por id
     *
     * @param  int $idequipo
     * @return \Illuminate\Http\Response
     */
    public function delete($idequipo)
    {
        $idequipo = (int) $idequipo;
        //Verificar que el idproyecto es de tipo integer
        if($idequipo === 0) {
            return response()->json(['message' => 'Tipo de dato no válido']);
        }

        $equipo = Equipo::where('equipo_id','=',$idequipo)->first();
        if($equipo && $equipo->estado){
            $equipo->estado = false;
            $equipo->save();
            return response()->json(['status' => 'Eliminado Correctamente']); 
        } else {
            return response()->json(['message' => 'Equipo no encontrada'], 404);
        }
    }

    public function getImagen($idequipo)
    {
      $equipo = Equipo::find($idequipo);
      if (!$equipo) {
          return response()->json(['message' => 'Equipo no encontrado'], 404);
      }

      $path = storage_path("app/{$equipo->imagen_equipo}");
      if (!File::exists($path)) {
          return response()->json(['message' => 'Imagen no encontrada'], 404);
      }

      $file = File::get($path);
      $type = File::mimeType($path);

      return response($file, 200)->header("Content-Type", $type);
    }

}
