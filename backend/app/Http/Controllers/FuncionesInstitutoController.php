<?php

namespace App\Http\Controllers;

use App\Models\FuncionesInstituto;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class FuncionesInstitutoController extends Controller
{
    //
    public function index(Request $request, $instituto_id)
    {
        // Verificar si se proporciona un instituto_id en la consulta
        // $instituto_id = $request->query('instituto_id')

        // Obtener todas las funciones o filtrar por instituto_id
        $funciones = $instituto_id
            ? FuncionesInstituto::where('instituto_id', $instituto_id)->get()
            : FuncionesInstituto::all();

        // Devolver las funciones como respuesta JSON
        return response()->json(['funciones' => $funciones]);
    }

    public function store(Request $request)
    {
        //Validamos que se este reciviendo la informacion pertinente
        $request->validate([
            'instituto_id' => 'required',
            'funcion' => 'required|string',
        ]);

        // Crear una nueva instancia del modelo y asignar valores individualmente
        $funcion = new FuncionesInstituto();
        $funcion->instituto_id = $request->instituto_id;
        $funcion->funcion = $request->funcion;

        // Guardar el modelo en la base de datos
        $funcion->save();

        // Retornar un mensaje en formato JSON
        return response()->json(['message' => 'Función creada con éxito']);
    }
    public function show($idinstituto)
    {
        // Obtener la función por ID con su relación con el instituto
        $funcion = FuncionesInstituto::with('instituto')
            ->where('funcion_id', '=', $idinstituto)->first();

        // Verificar si la función existe
        if (!$funcion) {
            return response()->json(['error' => 'Función no encontrada'], 404);
        }

        // Retornar los detalles de la función y su relación con el instituto en formato JSON
        return response()->json(['funcion' => $funcion]);
    }

    public function update(Request $request, $idfuncion)
    {
        $idfuncion = (int) $idfuncion;
        //Verificar que el idinstituto es de tipo integer
        if ($idfuncion === 0) {
            return response()->json(['message' => 'Tipo de dato no válido']);
        }
        $funcion = FuncionesInstituto::where('funcion_id', '=', $idfuncion)->first();
        if ($funcion) {
            try {
                $funcion->instituto_id = $request->instituto_id;
                $funcion->funcion = $request->funcion;
                $funcion->update();
                return response()->json(['status' => 'Actualizado Correctamente']);
            } catch (QueryException $e) {
                return response()->json(['message' => 'La funcion ya esta registrado']);
            }
        } else {
            return response()->json(['message' => 'Funcion no encontrado'], 404);
        }
    }

    public function delete($idfuncion)
    {
        $idfuncion = (int) $idfuncion;
        //Verificar que el idproyecto es de tipo integer
        if($idfuncion === 0) {
            return response()->json(['message' => 'Tipo de dato no válido']);
        }
        //Hacemos la consulta a la base de datos
        $funcion = FuncionesInstituto::where('funcion_id','=',$idfuncion)->first();
        if($funcion && $funcion->estado){
            $funcion->estado = false; //Cambiamos el estado a false
            $funcion->save();//Guardamos los datos
            return response()->json(['status' => 'Eliminado Correctamente']); //Eviamos mensaje de eliminacion correcta
        } else {
            return response()->json(['message' => 'Función no encontrada'], 404);
        }
    }
}
