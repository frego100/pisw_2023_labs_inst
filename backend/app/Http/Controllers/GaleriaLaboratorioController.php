<?php

namespace App\Http\Controllers;

use App\Models\GaleriaLaboratorio;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Storage;

class GaleriaLaboratorioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($idlaboratorio)
    {
        // Se obtiene la lista de registros de galeria de un laboratorio
        $galeriaLaboratorios = GaleriaLaboratorio::where('registro_id', $idlaboratorio)
        ->where('estado', true)->get();

        // Se retorna el array como respuesta
        return response()->json(['galeria' => $galeriaLaboratorios]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // Se crea un nuevo objeto GaleriaLaboratorio y se setean los valores ingresados en el request
        try {
            $galeria = new GaleriaLaboratorio;
            $galeria->registro_id = $request->registro_id;
            $galeria->nombre_imagen = $request->nombre_imagen;
            $galeria->descripcion = $request->descripcion;
            $galeria->estado = true;

            // Se verifica si se envio una imagen
            if($request->hasFile('imagen_galeria')) {
                // Guarda la imagen en el servidor
                $path = $request->file('imagen_galeria')->store('public/imagenes/galeriaLaboratorio');

                // Se asigna al registro de la db la ruta de la imagen en el servidor
                $galeria->imagen_galeria = substr($path, 7);
            }

            // Se guardan los registros
            $galeria->save();

            // Se retorna una respuesta
            return response()->json(['message' => 'Registro creado correctamente!'], 200);

        } catch (QueryException $e) {

            // Se retorna un error en caso haya alguno
            return response()->json(['message'=>'Ocurrio un error', 'error'=>$e]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\GaleriaLaboratorio  $galeriaLaboratorio
     * @return \Illuminate\Http\Response
     */
    public function show($idGaleriaLaboratorio)
    {
        //Verificar que el idgaleria se integer
        $idGaleriaLaboratorio = (int) $idGaleriaLaboratorio;
        if( $idGaleriaLaboratorio === 0) {
            return response()->json(['message'=> 'Tipo de dato no valido']);
        }

        // Obtener el registro de galeria laboratorio por ID
        $galeriaLaboratorio = GaleriaLaboratorio::where('galeria_id', $idGaleriaLaboratorio)->first();

        // Retornar respuesta segun el resultado de la busqueda
        if($galeriaLaboratorio) {
            return response()->json(['galeriaLaboratorio'=> $galeriaLaboratorio], 200);
        } else {
            return response()->json(['message' => 'Registro no encontrado'], 404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\GaleriaLaboratorio  $galeriaLaboratorio
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $idGaleriaLaboratorio)
    {
        //Verificar que el idgaleria se integer
        $idGaleriaLaboratorio = (int) $idGaleriaLaboratorio;
        if( $idGaleriaLaboratorio === 0) {
            return response()->json(['message'=> 'Tipo de dato no valido']);
        }

        // Buscamos el registro de la galeria por id
        $galeriaLaboratorio = GaleriaLaboratorio::where('galeria_id', $idGaleriaLaboratorio)->first();

        try {

            $galeriaLaboratorio->registro_id = $request->registro_id;
            $galeriaLaboratorio->nombre_imagen = $request->nombre_imagen;
            $galeriaLaboratorio->descripcion = $request->descripcion;

            // Se verifica si se envio una imagen
            if($request->hasFile('imagen_galeria')) {
                //Eliminamos primero la imagen
                Storage::delete('public/' . $galeriaLaboratorio->imagen_galeria);
                // Guarda la imagen en el servidor
                $path = $request->file('imagen_galeria')->store('public/imagenes/galeriaLaboratorio');

                // Se asigna al registro de la db la ruta de la imagen en el servidor
                $galeriaLaboratorio->imagen_galeria = substr($path, 7);
            }

            // Se guardan los registros
            $galeriaLaboratorio->update();

            // Se retorna una respuesta
            return response()->json(['message' => 'Actualizado correctamente!']);

        } catch (QueryException $e){
            return response()->json(['message'=>'Ocurrio un error', 'error'=>$e]);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\GaleriaLaboratorio  $galeriaLaboratorio
     * @return \Illuminate\Http\Response
     */
    public function destroy($idGaleriaLaboratorio)
    {
        //Verificar que el idgaleria se integer
        $idGaleriaLaboratorio = (int) $idGaleriaLaboratorio;
        if( $idGaleriaLaboratorio === 0) {
            return response()->json(['message'=> 'Tipo de dato no valido']);
        }

        // Buscamos el registro de la galeria por el id
        $galeriaLaboratorio = GaleriaLaboratorio::where('galeria_id', $idGaleriaLaboratorio)->first();

        if($galeriaLaboratorio && $galeriaLaboratorio->estado) {
            $galeriaLaboratorio->estado = false;
            $galeriaLaboratorio->save();
            return response()->json(['message'=>'Eliminado correctamente']);
        } else {
            return response()->json(['message'=>'Equipo no encontrado'], 404);
        }
    }
}
