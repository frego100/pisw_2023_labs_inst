<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\Models\User;

class GoogleAuthController extends Controller
{
  public function verifyGoogleToken(Request $request)
  {
    $accessToken = $request->input('token');

    // Verificar el token con la API de Google


    $googleResponse = Http::get('https://oauth2.googleapis.com/tokeninfo', [
      'access_token' => $accessToken,
    ]);

    if ($googleResponse->failed()) {
      return response()->json(['success' => false, 'error' => 'no llega'], 401);
    }

    $userData = $googleResponse->json();


    // Aquí puedes realizar acciones adicionales, como buscar o crear un usuario en tu base de datos
    // Por ejemplo, supongamos que estás utilizando el correo electrónico como identificador único
    $user = User::where('correo', $userData['email'])->first();

    if (!$user) {
      // El usuario no existe, puedes crearlo
      /*
            $user = new User([
                'email' => $userData['email'],
                // Otros campos según tu modelo de usuario
            ]);
            $user->save();
            */
      return response()->json(['fail' => true, 'message' => "User no registrado"]);
    }

    $responseData = [
      'correo' => $user->correo,
      'rol_id' => $user->rol_id,
      'token' => $user->createToken('token-name', ['*'])->plainTextToken
    ];


    // Puedes personalizar esta respuesta según tus necesidades
    return response()->json(['success' => true, 'userData' => $responseData]);
  }
}
