<?php

namespace App\Http\Controllers;

use App\Models\Instituto;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;

class InstitutoController extends Controller
{

    /**
     * Mostrar institutos de un director
     *
     * @return \Illuminate\Http\Response
     */
    public function indexByDirector($usuario_director)
    {
        $institutos = Instituto::where('usuario_director', '=', $usuario_director)
            ->orderBy('instituto_id', 'asc')
            ->with('director')
            ->get();
        return response()->json(['institutos' => $institutos]);
    }

    /**
     * Mostrar institutos
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $institutos = Instituto::orderBy('instituto_id', 'asc')
            ->with('director')
            ->get();
        return response()->json(['institutos' => $institutos]);
    }

    /**
     * Registrar instituto
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // Validamos el request
        $rules = [
            'usuario_director' => 'required|exists:users,usuario_id',
        ];

        /* obteniendo el resultado de las validaciones aplicadas a todos los parametros del objeto */
        $validator = Validator::make($request->all(), $rules);

        /* comprobando si alguna validación no se cumplió */
        if ($validator->fails()) {
            return response()->json(['message' => 'Error de validacion', 'errors' => $validator->errors()], 422);
        }

        try {
            $instituto = new Instituto;
            $instituto->nombre = $request->nombre;
            $instituto->mision = $request->mision;
            $instituto->vision = $request->vision;
            $instituto->historia = $request->historia;
            $instituto->usuario_director = $request->usuario_director; //usuario_id
            $instituto->comite_directivo = $request->comite_directivo;
            $instituto->contacto = $request->contacto;
            $instituto->ubicacion = $request->ubicacion;
            $instituto->url_instituto = $request->url_instituto;
            $instituto->url_facebook = $request->url_facebook;
            // Manejar la imagen
            if ($request->hasFile('imagen_instituto')) {
                $path = $request->file('imagen_instituto')->store('public/imagenes/institutos');
                $instituto->imagen_instituto = substr($path, 7);
            }
            $instituto->save();
            return response()->json(['status' => 'Creado Correctamente']);
        } catch (QueryException $e) {
            return response()->json(['message' => 'El Instituto ya esta registrado']);
        }
    }

    /**
     * Mostrar instituto por id
     *
     * @param  int $idinstituto
     * @return \Illuminate\Http\Response
     */
    public function show($idinstituto)
    {
        $idinstituto = (int) $idinstituto;
        //Verificar que el idinstituto es de tipo integer
        if ($idinstituto === 0) {
            return response()->json(['message' => 'Tipo de dato no válido']);
        }

        $instituto = Instituto::where('instituto_id', '=', $idinstituto)->first();
        if ($instituto && $instituto->estado) {
            return response()->json(['instituto' => $instituto]);
        } else {
            return response()->json(['message' => 'Instituto no encontrado'], 404);
        }
    }

    /**
     * Actualizar instituto  por id
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int $idinstituto
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $idinstituto)
    {

        // Validamos el request
        $rules = [
            'nombre' => 'required|string',
            /*'mision' => 'required|string',
            'vision' => 'required|string',
            'historia' => 'required|string',
            'contacto' => 'required|string',
            'ubicacion' => 'required|string',
            'url_instituto' => 'required|string|url',
            'url_facebook' => 'required|string|url'*/
        ];

        /* obteniendo el resultado de las validaciones aplicadas a todos los parametros del objeto */
        $validator = Validator::make($request->all(), $rules);

        /* comprobando si alguna validación no se cumplió */
        if ($validator->fails()) {
            return response()->json(['message' => 'Error de validacion', 'errors' => $validator->errors()], 422);
        }

        $idinstituto = (int) $idinstituto;
        //Verificar que el idinstituto es de tipo integer
        if ($idinstituto === 0) {
            return response()->json(['message' => 'Tipo de dato no válido']);
        }
        $instituto = Instituto::where('instituto_id', '=', $idinstituto)->first();
        if ($instituto && $instituto->estado) {
            try {
                $instituto->nombre = $request->nombre;
                /*$instituto->mision = $request->mision;
                $instituto->vision = $request->vision;
                $instituto->historia = $request->historia;*/
                //$instituto->usuario_director = $request->usuario_director;//usuario_id
                /*$instituto->comite_directivo = $request->comite_directivo;
                $instituto->contacto = $request->contacto;
                $instituto->ubicacion = $request->ubicacion;
                $instituto->url_instituto = $request->url_instituto;
                $instituto->url_facebook = $request->url_facebook;
                // Manejar la imagen
                if ($request->hasFile('imagen_instituto')) {
                    $path = $request->file('imagen_instituto')->store('public/imagenes/institutos');
                    $instituto->imagen_instituto = substr($path, 7);
                }*/
                $instituto->update();
                return response()->json(['status' => 'Actualizado Correctamente']);
            } catch (QueryException $e) {
                return response()->json(['message' => 'El Instituto ya esta registrado']);
            }
        } else {
            return response()->json(['message' => 'Instituto no encontrado'], 404);
        }
    }

    //Actualiza el registro de asignar director
    public function updateDirector(Request $request, $idinstituto)
    {

        // Validamos el request
        $rules = [
            'nombre' => 'required|string',
            /*'mision' => 'required|string',
            'vision' => 'required|string',
            'historia' => 'required|string',
            'contacto' => 'required|string',
            'ubicacion' => 'required|string',
            'url_instituto' => 'required|string|url',
            'url_facebook' => 'required|string|url'*/
        ];

        /* obteniendo el resultado de las validaciones aplicadas a todos los parametros del objeto */
        $validator = Validator::make($request->all(), $rules);

        /* comprobando si alguna validación no se cumplió */
        if ($validator->fails()) {
            return response()->json(['message' => 'Error de validacion', 'errors' => $validator->errors()], 422);
        }

        $idinstituto = (int) $idinstituto;
        //Verificar que el idinstituto es de tipo integer
        if ($idinstituto === 0) {
            return response()->json(['message' => 'Tipo de dato no válido']);
        }
        $instituto = Instituto::where('instituto_id', '=', $idinstituto)->first();
        if ($instituto && $instituto->estado) {
            try {
                $instituto->nombre = $request->nombre;
                /*$instituto->mision = $request->mision;
                $instituto->vision = $request->vision;
                $instituto->historia = $request->historia;*/
                $instituto->usuario_director = $request->usuario_director; //usuario_id
                /*$instituto->comite_directivo = $request->comite_directivo;
                $instituto->contacto = $request->contacto;
                $instituto->ubicacion = $request->ubicacion;
                $instituto->url_instituto = $request->url_instituto;
                $instituto->url_facebook = $request->url_facebook;
                // Manejar la imagen
                if ($request->hasFile('imagen_instituto')) {
                    $path = $request->file('imagen_instituto')->store('public/imagenes/institutos');
                    $instituto->imagen_instituto = substr($path, 7);
                }*/
                $instituto->update();
                return response()->json(['status' => 'Actualizado Correctamente']);
            } catch (QueryException $e) {
                return response()->json(['message' => 'El Instituto ya esta registrado']);
            }
        } else {
            return response()->json(['message' => 'Instituto no encontrado'], 404);
        }
    }


    /**
     * Eliminar instituto por id
     *
     * @param  int $idinstituto
     * @return \Illuminate\Http\Response
     */
    public function delete($idinstituto)
    {
        $idinstituto = (int) $idinstituto;
        //Verificar que el idinstituto es de tipo integer
        if ($idinstituto === 0) {
            return response()->json(['message' => 'Tipo de dato no válido']);
        }

        $instituto = Instituto::where('instituto_id', '=', $idinstituto)->first();
        if ($instituto) {
            // if($instituto && $instituto->estado){
            $instituto->estado = false;
            $instituto->save();
            return response()->json(['status' => 'Eliminado Correctamente']);
        } else {
            return response()->json(['message' => 'Instituto no encontrado'], 404);
        }
    }

    public function getImagen($idinstituto)
    {
        $instituto = Instituto::find($idinstituto);
        if (!$instituto) {
            return response()->json(['message' => 'Instituto no encontrado'], 404);
        }

        $path = storage_path("app/{$instituto->imagen_instituto}");
        if (!File::exists($path)) {
            return response()->json(['message' => 'Imagen no encontrada'], 404);
        }

        $file = File::get($path);
        $type = File::mimeType($path);

        return response($file, 200)->header("Content-Type", $type);
    }

    public function completarInstituto(Request $request, $idinstituto)
    {

        // Validamos el request
        $rules = [
            'mision' => 'required|string',
            'vision' => 'required|string',
            'historia' => 'required|string',
            'contacto' => 'required|string',
            'ubicacion' => 'required|string',
            'url_instituto' => 'required|string|url',
            'url_facebook' => 'required|string|url'
        ];

        /* obteniendo el resultado de las validaciones aplicadas a todos los parametros del objeto */
        $validator = Validator::make($request->all(), $rules);

        /* comprobando si alguna validación no se cumplió */
        if ($validator->fails()) {
            return response()->json(['message' => 'Error de validacion', 'errors' => $validator->errors()], 422);
        }

        //Verificar que el idinstituto es de tipo integer
        $idinstituto = (int) $idinstituto;
        if ($idinstituto === 0) {
            return response()->json(['message' => 'Tipo de dato no válido']);
        }

        // Obtenemos el instituto por el id
        $registroinstituto = Instituto::where('instituto_id', '=', $idinstituto)->first();

        if ($registroinstituto) {
            try {
                // Actualizamos los datos
                $registroinstituto->mision = $request->mision;
                $registroinstituto->vision = $request->vision;
                $registroinstituto->historia = $request->historia;
                $registroinstituto->contacto = $request->contacto;
                $registroinstituto->ubicacion = $request->ubicacion;
                $registroinstituto->url_instituto = $request->url_instituto;
                $registroinstituto->url_facebook = $request->url_facebook;
                $registroinstituto->nombre_imagen = $request->nombre_imagen;

                // Verificamos si se envía un comité directivo
                /*if ($request->comite_directivo) {
                    $registroinstituto->comite_directivo = $request->comite_directivo;
                }*/

                // Manejar la imagen
                if ($request->hasFile('imagen_instituto')) {
                    $path = $request->file('imagen_instituto')->store('public/imagenes/institutos');

                    $registroinstituto->imagen_instituto = substr($path, 7);
                }

                // Guardamos los datos en la db
                $registroinstituto->update();

                // Retornamos una respuesta correcta
                return response()->json(['status' => 'Actualizado correctamente']);
            } catch (QueryException $e) {
                // Retornamos el error
                return response()->json(['message' => $e]);
            }
        } else {
            // Retornamos que no se encontró el instituto
            return response()->json(['message' => 'Instituto no encontrado'], 404);
        }
    }

    // Listar institutos por director asignado
    public function VistaInstitutoDirector($id_director)
    {

        // Obtenemos la lista de institutos por id de director, incluendo la info del director
        $institutos = Instituto::where('usuario_director', '=', $id_director)->orderBy('instituto_id', 'asc')
            ->with('director')
            ->get();

        // Retornamos la info del instituto
        return response()->json(['institutos' => $institutos[0]], 200);
    }
}
