<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Laravel\Socialite\Facades\Socialite;


class LoginController extends Controller
{
    /**
     * Controlador para gestionar el login y logout
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function redirectToGoogle()
    {
        return Socialite::driver('google')->redirect();
    }

    public function handleGoogleCallback()
    {
        $user = Socialite::driver('google')->user();

        // Verificar si el usuario existe en la base de datos o crear uno nuevo
        $authUser = User::firstOrCreate([
            'correo' => $user->email,
        ], [
            'nombre' => $user->name,
            'contrasena' => Hash::make('random-password'), // Asigna una contraseña aleatoria o implementa una lógica segura
        ]);

        Auth::login($authUser, true);

        $token = $authUser->createToken('token-name')->plainTextToken;

        return response()->json(['token' => $token]);
    }

    public function login(Request $request)
    {
        $correo = $request->email;
        $passw = $request->password;
        // Verificando que el dominio del correo pertenezca a la universidad
        if (substr($correo, -12) === "@unsa.edu.pe") {
            // Buscando al usuario por el correo 
            // Si lo encuentra que lo obtenga con su objeto Rol interno
            $usuario = User::with('rol')->where('correo', $correo)->first();
            if ($usuario && $usuario->estado) {
                // if($passw === $usuario->password){ --> Comparaba la contraseña en texto plano con el hash | No funcionaba
                if (Hash::check($passw, $usuario->contrasena)) {
                    // if($passw === $usuario->contrasena){
                    $token = $usuario->createToken('token-name', ['*'])->plainTextToken;
                    $usuario->makeHidden('email_verified_at');
                    return response()->json(['usuario' => $usuario, 'token' => $token], 200);
                } else {
                    return response()->json(['message' => 'Contraseña incorrecta'], 400);
                }
            } else {
                return response()->json(['message' => 'Usuario no encontrado'], 404);
            }
        } else {
            return response()->json(['message' => 'Correo no perteneciente a la UNSA'], 400);
        }
    }

    public function logout(Request $request)
    {
        // Obtén el usuario autenticado actual
        $user = $request->user();

        // Elimina todos los tokens de acceso personal del usuario
        $user->tokens()->delete();

        // Si estás utilizando la opción de "recordar", también elimina el remember_token
        //$user->forceFill(['remember_token' => null])->save();

        // Retorna una respuesta exitosa
        return response()->json(['message' => 'Sesión cerrada exitosamente']);
    }

    //Codigo de ejemplo para probar Auth de Sanctum
    public function lista(Request $request)
    {
        return User::all();
    }
}
