<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Poi;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class PoiController extends Controller
{
    public function index($idinstituto)
    {
        $pois = Poi::where('instituto_id', '=', $idinstituto)->orderBy('id', 'asc')->get();
        return response()->json(['pois' => $pois]);
    }

    public function store(Request $request)
    {
        try {
            $poi = new Poi;
            $poi->instituto_id = $request->instituto_id;
            $poi->year = $request->year;
            $poi->nombre_poi = $request->nombre_poi;
            if ($request->hasFile('document_uri')) {
                $path = $request->file('document_uri')->store('public/documentos/pois');
                $poi->document_uri = $path;
            }
            $poi->save();

            return response()->json(['status' => 'POI Creado Correctamente']); 
        } catch (QueryException $e) {
            return response()->json(['message'=> 'Ocurrió un error al crear el poi', 'error'=> $e], 500);
        }
    }

    public function show($idpoi)
    {
        $idpoi = (int) $idpoi;
        if ($idpoi === 0) {
            return response()->json(['message' => 'ID de POI no válido'], 400);
        }

        $poi = Poi::find($idpoi);
        if ($poi) {
            return response()->json(['poi' => $poi]);
        } else {
            return response()->json(['message' => 'POI no encontrado'], 404);
        }
    }

    public function update(Request $request, $idpoi)
    {
        $idpoi = (int) $idpoi;
        if ($idpoi === 0) {
            return response()->json(['message' => 'ID de POI no válido'], 400);
        }

        $poi = Poi::where('id','=',$idpoi)->first();
        if ($poi) {
            try {
                $poi->instituto_id = $request->instituto_id;
                $poi->year = $request->year;
                $poi->nombre_poi = $request->nombre_poi;
                
                if ($request->hasFile('document_uri')) {
                    Storage::delete($poi->document_uri); // Eliminamos el archivo antiguo
                    $path = $request->file('document_uri')->store('public/documentos/pois'); // Guardamos el nuevo archivo
                    $poi->document_uri = $path;
                }

                $poi->update();
                return response()->json(['status' => 'POI Actualizado Correctamente']); 
            } catch (QueryException $e) {
                return response()->json(['message'=> 'Error al actualizar el poi', 'error'=> $e], 500);
            }
        } else {
            return response()->json(['message' => 'POI no encontrado'], 404);
        }
    }

    public function delete($idpoi)
    {
        $idpoi = (int) $idpoi;
        if ($idpoi === 0) {
            return response()->json(['message' => 'ID de POI no válido'], 400);
        }

        $poi = Poi::find($idpoi);
        if ($poi) {
            $poi->delete();
            return response()->json(['status' => 'POI Eliminado Correctamente']); 
        } else {
            return response()->json(['message' => 'POI no encontrado'], 404);
        }
    }

    public function download($idpoi)
    {
        $poi = Poi::find($idpoi);
        if (!$poi) {
            return response()->json(['message' => 'Punto de interés no encontrado'], 404);
        }

        $path = storage_path("app/{$poi->document_uri}");
        if (!File::exists($path)) {
            return response()->json(['message' => 'Archivo no encontrado'], 404);
        }

        try {
            $file = File::get($path);
            $type = File::mimeType($path);
            return response($file, 200)
                ->header('Content-Type', $type)
                ->header('Content-Disposition', 'attachment; filename="' . basename($path) . '"');
        } catch (\Exception $e) {
            return response()->json(['message'=> 'Error al descargar el archivo', 'error'=> $e], 500);
        }
    }

}
