<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Poi;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class PoiController extends Controller
{
    //

    public function index($idinstituto)
    {
        $pois = Poi::where('instituto_id','=',$idinstituto)->orderBy('poi_id', 'asc')->get();
        return response()->json(['pois' => $pois]);
    }

    public function store(Request $request)
    {
        try {
            $poi = new Poi;
            $poi->instituto_id = $request->instituto_id;
            $poi->año = $request->año;
            $poi->nombre_poi = $request->nombre_poi;
            
            // Manejar la imagen
            if ($request->hasFile('archivo_poi')) {
              $path = $request->file('archivo_poi')->store('public/imagenes/pois');
              $poi->archivo_poi = substr($path, 7);

              // $imagen = $request->file('imagen_equipo');
              // $imagenNombre = $imagen->getClientOriginalName();
              // $imagenBinaria = file_get_contents($imagen->getRealPath());
              // $proyecto->imagen_equipo = $imagenBinaria;
              //$proyecto->imagen_equipos_nombre = $imagenNombre;
            }
            
            $poi->save();

            return response()->json(['status' => 'Creado Correctamente']); 
        } catch (QueryException $e) {
          return response()->json(['message'=> 'Ocurrio un error', 'error'=> $e]);
        } catch (\Exception $e) {
            return response()->json(['message' => 'Error en la solicitud', 'error' => $e->getMessage()], 500);
        }
    }


    /**
     * Mostrar equipo por id
     *
     * @param  int $idproyecto
     * @return \Illuminate\Http\Response
     */
    public function show($idpoi)
    {
        $idpoi = (int) $idpoi;
        //Verificar que el idequipo es de tipo integer
        if($idpoi === 0) {
            return response()->json(['message' => 'Tipo de dato no válido']);
        }

        $poi = Poi::where('poi_id','=',$idpoi)->first();
        if($poi){
            return response()->json(['poi' => $poi]);
        } else {
            return response()->json(['message' => 'Equipo no encontrada'], 404);
        }
    }

    /**
     * Actualizar equipo  por id
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int $idproyecto
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $idpoi)
    {
        $idpoi = (int) $idpoi;
        //Verificar que el idproyecto es de tipo integer
        if($idpoi === 0) {
            return response()->json(['message' => 'Tipo de dato no válido']);
        }
        $poi = Poi::where('poi_id','=',$idpoi)->first();
        if($poi){
            try{
                // $equipo->registro_id = $request->registro_id;
                $poi->año = $request->año;
                $poi->nombre_poi = $request->nombre_poi;
                
                // Manejar la imagen
                if ($request->hasFile('archivo_poi')) {
                  Storage::delete($poi->archivo_poi); // Eliminamos el archivo antiguo
                  $path = $request->file('archivo_poi')->store('public/imagenes/pois');
                  $poi->archivo_poi = substr($path, 7);

                    // $imagen = $request->file('imagen_equipo');
                    // $imagenNombre = $imagen->getClientOriginalName();
                    // $imagenBinaria = file_get_contents($imagen->getRealPath());
                    // $poi->archivo_poi = $imagenBinaria;
                    // $proyecto->imagen_equipos_nombre = $imagenNombre;
                }
                $poi->update();
                return response()->json(['status' => 'Actualizado Correctamente']); 
            } catch (QueryException $e){
              return response()->json(['message'=> 'Ocurrio un error', 'error'=> $e]);
            }
        } else {
            return response()->json(['message' => 'Proyecto no encontrada'], 404);
        }
    }

    /**
     * Eliminar proyecto por id
     *
     * @param  int $idequipo
     * @return \Illuminate\Http\Response
     */
    public function delete($idpoi)
    {
        $idpoi = (int) $idpoi;
        //Verificar que el idproyecto es de tipo integer
        if($idpoi === 0) {
            return response()->json(['message' => 'Tipo de dato no válido']);
        }

        $poi = Poi::where('poi_id','=',$idpoi)->first();
        if($poi && $poi->estado){
            $poi->estado = false;
            $poi->save();
            return response()->json(['status' => 'Eliminado Correctamente']); 
        } else {
            return response()->json(['message' => 'Equipo no encontrada'], 404);
        }
    }

    public function getImagen($idpoi)
    {
      $poi = Poi::find($idpoi);
      if (!$poi) {
          return response()->json(['message' => 'Equipo no encontrado'], 404);
      }

      $path = storage_path("app/{$poi->archivo_poi}");
      if (!File::exists($path)) {
          return response()->json(['message' => 'Imagen no encontrada'], 404);
      }

      $file = File::get($path);
      $type = File::mimeType($path);

      return response($file, 200)->header("Content-Type", $type);
    }
}
