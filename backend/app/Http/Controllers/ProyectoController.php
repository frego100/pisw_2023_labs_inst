<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Proyecto;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class ProyectoController extends Controller
{
    //Funcion aara mostrar los proyectos por laboratorio
    public function index($idregistrarlaboratio)
    {
        //mostramos los proyectos que estan relacionados con el registro de laboratorio
        $proyectos = Proyecto::where('registro_id','=',$idregistrarlaboratio)->orderBy('proyecto_id', 'asc')->get();
        return response()->json(['proyectos' => $proyectos]);
    }

    public function store(Request $request)
    {
        try {
            $proyecto = new Proyecto;
            $proyecto->registro_id = $request->registro_id;
            $proyecto->nombre_imagen = $request->nombre_imagen;
            $proyecto->nombre_proyecto = $request->nombre_proyecto;
            $proyecto->investigador_principal = $request->investigador_principal;
            $proyecto->coinvestigadores = $request->coinvestigadores;
            $proyecto->etapa = $request->etapa;
            $proyecto->lineas_investigacion = $request->lineas_investigacion;
            $proyecto->duracion = $request->duracion;
            $proyecto->doi = $request->doi;
            $proyecto->resumen = $request->resumen;
            $proyecto->iba = $request->iba;
            if ($request->hasFile('imagen_referencial')) {
              $path = $request->file('imagen_referencial')->store('public/imagenes/proyectos');
              $proyecto->imagen_referencial = substr($path, 7);

                // $image = $request->file('imagen_referencial');
                // $imageName = time() . '.' . $image->getClientOriginalExtension();
                // $image->storeAs('images', $imageName, 'public'); // Guarda la imagen en la carpeta 'public/images'
                
                // // Registra la ruta en la base de datos
                // $proyecto->imagen_referencial = 'images/' . $imageName; // Ruta relativa a la carpeta de almacenamiento
                
            }            
            $proyecto->save();

            return response()->json(['status' => 'Creado Correctamente']); 
        } catch (QueryException $e) {
          return response()->json(['message'=> 'Ocurrio un error', 'error'=> $e]);
        }
    }


    /**
     * Mostrar proyecto por id
     *
     * @param  int $idproyecto
     * @return \Illuminate\Http\Response
     */
    public function show($idproyecto)
    {
        $idproyecto = (int) $idproyecto;
        //Verificar que el idproyecto es de tipo integer
        if($idproyecto === 0) {
            return response()->json(['message' => 'Tipo de dato no válido']);
        }

        $proyecto = Proyecto::where('proyecto_id','=',$idproyecto)->first();
        if($proyecto){
            return response()->json(['proyecto' => $proyecto]);
        } else {
            return response()->json(['message' => 'Proyecto no encontrada'], 404);
        }
    }

    /**
     * Actualizar proyecto  por id
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int $idproyecto
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $idproyecto)
    {
        $idproyecto = (int) $idproyecto;
        //Verificar que el idproyecto es de tipo integer
        if($idproyecto === 0) {
            return response()->json(['message' => 'Tipo de dato no válido']);
        }
        //Validamos que el id sea el mismo y cogemos el primero
        $proyecto = proyecto::where('proyecto_id','=',$idproyecto)->first();
        if($proyecto){
            try{
                $proyecto->nombre_imagen = $request->nombre_imagen;
                $proyecto->nombre_proyecto = $request->nombre_proyecto;
                $proyecto->investigador_principal = $request->investigador_principal;
                $proyecto->coinvestigadores = $request->coinvestigadores;
                $proyecto->etapa = $request->etapa;
                $proyecto->duracion = $request->duracion;
                $proyecto->lineas_investigacion = $request->lineas_investigacion;
                $proyecto->doi = $request->doi;
                $proyecto->resumen = $request->resumen;
                $proyecto->iba = $request->iba;
                

                if($request->hasFile('imagen_referencial')) {
                  Storage::delete($proyecto->imagen_referencial); // Eliminamos el archivo antiguo
                  $path = $request->file('imagen_referencial')->store('public/imagenes/proyectos'); // Guardamos el nuevo archivom
                  $proyecto->imagen_referencial = substr($path, 7);
                }

                $proyecto->update();
                return response()->json(['status' => 'Actualizado Correctamente']); 
            } catch (QueryException $e){
                return response()->json(['message'=> 'Ocurrio un error', 'error'=> $e]);
            }
        } else {
            return response()->json(['message' => 'Proyecto no encontrada'], 404);
        }
    }

    /**
     * Eliminar proyecto por id cambiando el estado a false
     *
     * @param  int $idproyecto
     * @return \Illuminate\Http\Response
     */
    public function delete($idproyecto)
    {
        $idproyecto = (int) $idproyecto;
        //Verificar que el idproyecto es de tipo integer
        if($idproyecto === 0) {
            return response()->json(['message' => 'Tipo de dato no válido']);
        }

        $proyecto = proyecto::where('proyecto_id','=',$idproyecto)->first();
        if($proyecto && $proyecto->estado){
            $proyecto->estado = false; //Cambiamos el estado a false
            $proyecto->save();
            return response()->json(['status' => 'Eliminado Correctamente']); 
        } else {
            return response()->json(['message' => 'Proyecto no encontrada'], 404);
        }
    }

    public function getImagen($idproyecto)
    {
      $proyecto = Proyecto::find($idproyecto);
      if (!$proyecto) {
          return response()->json(['message' => 'Proyecto no encontrado'], 404);
      }

      $path = storage_path("app/{$proyecto->imagen_referencial}");
      if (!File::exists($path)) {
          return response()->json(['message' => 'Imagen no encontrada'], 404);
      }

      $file = File::get($path);
      $type = File::mimeType($path);

      return response($file, 200)->header("Content-Type", $type);
    }

    public function listarPublicaciones($idregistrarlaboratorio) {

      $idregistrarlaboratorio = (int) $idregistrarlaboratorio;
        //Verificar que el idlaboratorio es de tipo integer
      if($idregistrarlaboratorio === 0) {
          return response()->json(['message' => 'Tipo de dato no válido']);
      }
      
      // Se obtienen las publicaciones por laboratorio
      $publicaciones = Proyecto::where('registro_id', $idregistrarlaboratorio)->where('etapa','=','Cierre')->orderBy('proyecto_id', 'asc')
                                  ->get();

      if(count($publicaciones) == 0) {
        return response()->json(['message' => 'El laboratorio no tiene publicaciones' ], 404);
      }

      return response()->json(['publicaciones'=>$publicaciones], 200);
    }

    // Funcion para cambiar la etapa del proyecto
    public function cambiarEtapa($proyectoId, Request $request)
    {

        // Validamos el request
        $rules = [
            'etapa' => 'required|string'
        ];

        /* obteniendo el resultado de las validaciones aplicadas a todos los parametros del objeto */
        $validator = Validator::make($request->all(), $rules);

        /* comprobando si alguna validación no se cumplió */
        if($validator->fails()){
          return response()->json(['message'=>'Error de validacion', 'errors'=>$validator->errors()], 422);
        }

        try {
            $proyecto = Proyecto::find($proyectoId);

            // Verificamos que el proyecto exista
            if (!$proyecto) {
                return response()->json(['message' => 'Proyecto no encontrado'], 404);
            }

            // Se cambia la etapa en el objeto
            $proyecto->etapa = $request->etapa;

            // Se guardan los cambios en la db
             $proyecto->update();

            // Retorna una respuesta correcta
            return response()->json(['message' => 'Etapa agregada correctamente'], 200);
        } catch (\Exception $e) {
            // Retorna el error
            return response()->json(['message' => 'Error al agregar la etapa', 'error' => $e], 500);
        }
    }
}
