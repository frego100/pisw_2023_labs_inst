<?php

namespace App\Http\Controllers;

use App\Models\PublicacionInstituto;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PublicacionInstitutoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($idinstituto)
    {
        // Verificamos que el id del laboratorio sea numerico
        $idinstituto = (int) $idinstituto;
        
        if($idinstituto === 0) {
            return response()->json(['message' => 'Tipo de dato no válido']);
        }
        
        // Obtenemos las publicaciones
        $publicaciones = PublicacionInstituto::where('instituto_id','=',$idinstituto)
                                    ->orderBy('publicacion_id', 'asc')
                                    ->with('instituto')
                                    ->get();

        // Verificar si existen publicaciones
        if(count($publicaciones) == 0) {
            return response()->json(['message' => 'No cuenta con publicaciones' ], 404);
        }

        // Retornar las publicaciones
        return response()->json(['publicaciones'=>$publicaciones], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Validamos lso valores necesarios
        $request->validate([
            'titulo' => 'required|string',
            'link' => 'required|string',
            'instituto_id' => 'required|integer'
        ]);

        try {
            // Seteamos un objeto nuevo con valores de la peticion
            $publicacion = new PublicacionInstituto;
            $publicacion->titulo = $request->titulo;
            $publicacion->link = $request->link;
            $publicacion->instituto_id = $request->instituto_id;
            $publicacion->estado = true;

            // Guardamos el valor en la db
            $publicacion->save();

            // Retornamos operacion exitosa
            return response()->json(['status'=> 'Creado correctamente'], 200);

        } catch (QueryException $e){
            // Devolvemos el error en caso haya uno
            return response()->json(['message'=>'Ocurrio un error', 'error'=> $e]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PublicacionInstitutos  $publicacionInstitutos
     * @return \Illuminate\Http\Response
     */
    public function show($idpublicacion)
    {
        // Verificamos que el id del laboratorio sea numerico
        $idpublicacion = (int) $idpublicacion;
        
        if($idpublicacion === 0) {
            return response()->json(['message' => 'Tipo de dato no válido']);
        }

        //Obtenemos la publicacion
        $publicacion = PublicacionInstituto::where('publicacion_id', $idpublicacion)->first();

        // Verificamos que la publicacion buscada exista
        if($publicacion){
            return response()->json(['publicacion'=> $publicacion], 200);
        }

        // Retornamos que no hay la publicacion
        return response()->json(['message' => 'Publicacion no encontrada'], 404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PublicacionInstitutos  $publicacionInstitutos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $idpublicacion)
    {
        // Verificamos que el id del laboratorio sea numerico
        $idpublicacion = (int) $idpublicacion;
        
        if($idpublicacion === 0) {
            return response()->json(['message' => 'Tipo de dato no válido']);
        }

        //Obtenemos la publicacion
        $publicacion = PublicacionInstituto::where('publicacion_id', $idpublicacion)->first();

        try {
            // Seteamos un objeto nuevo con valores de la peticion
            $publicacion->titulo = $request->titulo;
            $publicacion->link = $request->link;
            $publicacion->instituto_id = $request->instituto_id;

            // Guardamos el valor en la db
            $publicacion->update();

            // Retornamos operacion exitosa
            return response()->json(['status'=> 'Actualizado correctamente'], 200);

        } catch (QueryException $e){
            // Devolvemos el error en caso haya uno
            return response()->json(['message'=>'Ocurrio un error', 'error'=> $e]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PublicacionInstitutos  $publicacionInstitutos
     * @return \Illuminate\Http\Response
     */
    public function destroy($idpublicacion)
    {
        // Verificamos que el id del laboratorio sea numerico
        $idpublicacion = (int) $idpublicacion;
        
        if($idpublicacion === 0) {
            return response()->json(['message' => 'Tipo de dato no válido']);
        }

        // Obtenemos la publicacion
        $publicacion = PublicacionInstituto::where('publicacion_id','=',$idpublicacion)->first();

        // Verificamos que exista la publicacion y tenga el estado true
        if($publicacion && $publicacion->estado){

            $publicacion->estado = false; //Cambiamos el estado a false

            // Guardamos los cambios en la db
            $publicacion->save();

            // Retornamos operacion exitosa
            return response()->json(['status' => 'Eliminado Correctamente']); 
        } else {
            return response()->json(['message' => 'Publicacion no encontrada'], 404);
        }
    }
}
