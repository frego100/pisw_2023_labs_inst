<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Laboratorio;
use App\Models\User;
use App\Models\Area;
use App\Models\Disciplina;
use App\Models\RegistroLaboratorio;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class RegistroLaboratorioController extends Controller
{

  //LISTAR LOS LABORATORIOS
  public function VisLabCoordinador($idcoordinador)
  {
    
    $labs = RegistroLaboratorio::where('coordinador_id','=',$idcoordinador)->orderBy('registro_id', 'asc')
                                ->with('coordinador')
                                ->with('laboratorio')
                                ->with('area')
                                //->with('disciplina')
                                ->with('disciplinas')
                                ->get();

    return response()->json(['coordLabs'=>$labs], 200);

  }

  public function VisLab()
  {
    
    $labs = RegistroLaboratorio::orderBy('registro_id', 'asc')
                                ->with('coordinador')
                                ->with('laboratorio')
                                ->with('area')
                                //->with('disciplina')
                                //->with('disciplinas')
                                ->get();

    return response()->json(['Labs'=>$labs], 200);

  }

  //OBTENER LABORATORIO POR ID LABORATORIO
  public function getLaboratorioByIdLaboratorioPublic($idLaboratorio)
  {
    $idLaboratorio = (int) $idLaboratorio;
        //Verificar que el idlaboratorio es de tipo integer
        if($idLaboratorio === 0) {
            return response()->json(['message' => 'Tipo de dato no válido']);
        }
    
    $labs = RegistroLaboratorio::where('registro_id','=',$idLaboratorio)->orderBy('registro_id', 'asc')
                                ->with('coordinador')
                                ->get();

    return response()->json(['laboratorio'=>$labs], 200);

  }

   // Para visualizar todos los laboratorios 
  // public function VisLabCoordinadorpublic(Request $request)
  public function VisLabCoordinadorpublic()
  {

    $labs = RegistroLaboratorio::with('coordinador')
                                ->with('laboratorio')
                                ->with('area')
                                ->with('disciplinas')
                                ->where('estado', true)
                                ->get();

    return response()->json(['publiclabs'=>$labs], 200);

  }


  //CRUD PARA REGISTRO LABORATORIOS
  //Funcion para regitrar la asignación de responsable a un laboratorio
  public function store(Request $request)
  {
    try {
      $request->validate([
        'coordinador_id' => 'required|integer',
        'laboratorio_id' => 'required|integer',
        'area_id' => 'required|integer',
        //'disciplina_id' => 'required|integer',
        'disciplinas' => 'required|array',
        'ubicacion' => 'required|string|max:255',
      ]);

      $registroLaboratorio = new RegistroLaboratorio;
      $registroLaboratorio->coordinador_id = $request->coordinador_id;
      $registroLaboratorio->laboratorio_id = $request->laboratorio_id;
      $registroLaboratorio->area_id = $request->area_id;
      //$RegistroLaboratorio->disciplina_id = $request->disciplina_id;
      $registroLaboratorio->ubicacion = $request->ubicacion;
      if($request->servicios){
        $RegistroLaboratorio->servicios = $request->servicios;
      }
      $registroLaboratorio->save();
      // Sincronizar las disciplinas
      $registroLaboratorio->disciplinas()->sync($request->disciplinas);
      return response()->json(['status' => 'Creado Correctamente']); 
      } catch (\Exception $e) {
        return response()->json(['error' => $e->getMessage()], 500);
    }
  }

  /*Funcion para mandar datos a los select de la vista de registro de laboratorios */
  public function VistaRegLab(){
  
    $responsables= User::where('rol_id','=',2)->get();
    $laboratorios= Laboratorio::select('laboratorio_id', 'nombre','estado')->get();
    $areas= Area::select('area_id','nombre','estado')->get();
    $disciplinas=Disciplina::select('disciplina_id','nombre','estado')->get();

    return response()->json(['responsables' => $responsables,'laboratorios' => $laboratorios,'areas' => $areas,'disciplinas' => $disciplinas],200);
    
  }
 //Elimina el registro de asignación de laboratorio
 public function delete($idregistrolaboratorio){
  $idregistrolaboratorio = (int) $idregistrolaboratorio;
  //Verificar que el id es de tipo integer
  if($idregistrolaboratorio === 0) {
      return response()->json(['message' => 'Tipo de dato no válido']);
  }

  $regLaboratorio = RegistroLaboratorio::where('registro_id','=',$idregistrolaboratorio)->first();
  if($regLaboratorio && $regLaboratorio->estado){
      $regLaboratorio->estado = false;
      $regLaboratorio->save();
      return response()->json(['status' => 'Eliminado Correctamente']); 
  } else {
      return response()->json(['message' => 'Registro Laboratorio no encontrado'], 404);
  }
 }
 //Actualiza el registro de laboratorio
 public function update(Request $request, $idregistrolaboratorio){

  $request->validate([
    'coordinador_id' => 'required|integer',
    'laboratorio_id' => 'required|integer',
    'area_id' => 'required|integer',
    //'disciplina_id' => 'required|integer',
    'disciplinas' => 'required|array',
    'ubicacion' => 'required|string|max:255',
  ]);
  
  $idregistrolaboratorio = (int) $idregistrolaboratorio;
  //Verificar que el id es de tipo integer
  if($idregistrolaboratorio === 0) {
      return response()->json(['message' => 'Tipo de dato no válido']);
  }

  //$regLaboratorio = RegistroLaboratorio::where('registro_id','=',$idregistrolaboratorio)->first();
  $registroLaboratorio = RegistroLaboratorio::find($idregistrolaboratorio);
      if($registroLaboratorio){
          try{
              $registroLaboratorio->coordinador_id = $request->coordinador_id;
              $registroLaboratorio->laboratorio_id = $request->laboratorio_id;
              $registroLaboratorio->area_id = $request->area_id;
              //$regLaboratorio->disciplina_id = $request->disciplina_id;
              $registroLaboratorio->ubicacion = $request->ubicacion;
              if($request->servicios){
                $registroLaboratorio->servicios = $request->servicios;
              }
              // $regLaboratorio->mision = $request->get('mision');
              // $regLaboratorio->vision = $request->get('vision');
              // $regLaboratorio->historia = $request->get('historia');
              // $regLaboratorio->estado = $request->get('estado');
              //$regLaboratorio->update();
              $registroLaboratorio->save();
              // Sincronizar las disciplinas
              $registroLaboratorio->disciplinas()->sync($request->disciplinas);
              return response()->json(['status' => 'Actualizado Correctamente']); 
          } catch (QueryException $e){
              return response()->json(['message'=> 'Ocurrio un error', 'error'=> $e]);
          }
      } else {
          return response()->json(['message' => 'Registro de Laboratorio no encontrado'], 404);
      }
 }

    // public function index (){
    //     try{
    //         $lab_publics=Laboratorio::where('estado',true)->where('',)->orderBy('laboratorio_id','asc')->get();
    //         return response()->json(['registro_laboratorios' => $lab_publics]);
    //     }
    //     catch(QueryException $e){
    //         return response()->json(['message'=> 'La disciplina ya esta registrada']);
    //     }
    // }
    
    public function completar(Request $request, $idregistrarlaboratio){

      $request->validate([
        'mision' => 'required|string',
        'vision' => 'required|string',
        'historia' => 'required|string',
      ]);

      // $idusuario=(new UserController) -> getUser($request)->usuario_id;
      if($idregistrarlaboratio === 0) {
          return response()->json(['message' => 'Tipo de dato no válido']);
      }
      $registrolaboratorio = RegistroLaboratorio::where('registro_id','=',$idregistrarlaboratio)->first();
      if($registrolaboratorio){
          try{
              // $registrolaboratorio->ubicacion = $request->get('ubicacion');
              $registrolaboratorio->mision = $request->mision;
              $registrolaboratorio->vision = $request->vision;
              $registrolaboratorio->historia = $request->historia;
              $registrolaboratorio->update();
              return response()->json(['status' => 'Actualizado Correctamente']); 
          } catch (QueryException $e){
              return response()->json(['message'=> 'El laboratorio ya está registrada']);
          }
      } else {
          return response()->json(['message' => 'Laboratorio no encontrada'], 404);
      }
    }

    // public function indexMylabs(){
    //     try{
    //         $lab_publics=Laboratorio::where('estado',true)->where('',)->orderBy('laboratorio_id','asc')->get();
    //         return response()->json(['registro_laboratorios' => $lab_publics]);
    //     }
    //     catch(QueryException $e){
    //         return response()->json(['message'=> 'La disciplina ya esta registrada']);
    //     }
    // }


    // Función para listar todos los servicios
    public function listarServicios($idregistrarlaboratorio) {
      $idregistrarlaboratorio = (int) $idregistrarlaboratorio;
        //Verificar que el idlaboratorio es de tipo integer
      if($idregistrarlaboratorio === 0) {
          return response()->json(['message' => 'Tipo de dato no válido']);
      }
      
      $lab = RegistroLaboratorio::where('registro_id','=',$idregistrarlaboratorio)->orderBy('registro_id', 'asc')
                                  ->get();

      if(count($lab) == 0) {
        return response()->json(['message' => 'Laboratorio no encontrado' ], 404);
      } else {
        if(!$lab[0]->servicios) {
          $lab[0]->servicios = [];
        }
      }

      return response()->json(['servicios'=>$lab[0]->servicios], 200);
    }

    // Función para ingresar un nuevo servicio en el laboratorio
    public function ingresarServicio(Request $request, $idregistrarlaboratio){
        
        // Validamos los valores necesarios
        $rules = [
            // 'servicios' => 'required|string',
            'servicios' => 'required|array',
        ];

        $validator = Validator::make($request->all(), $rules);

        /* comprobando si alguna validación no se cumplió */
        if($validator->fails()){
          return response()->json(['message'=>'Error de validacion', 'errors'=>$validator->errors()], 422);
        }
        
        // Verificamos que el id del laboratorio sea numerico
        $idregistrarlaboratio = (int) $idregistrarlaboratio;

        if($idregistrarlaboratio === 0) { // Cuando se parsea una cadena a string siempre da 0
            return response()->json(['message' => 'Tipo de dato no válido']);
        }

        // Obtenemos el laboratorio en el que ingresar el nuevo servicio
        $lab = RegistroLaboratorio::where('registro_id', '=', $idregistrarlaboratio)->first();

        // Verificamos que exista el laboratorio
        if($lab) {

            // Asignamos los servicios
            $servicios = $lab->servicios ?? [];

            // Agregamos el nuevo servicio en el arreglo de servicios
            // array_push($lab->servicios, $request->servicios);

            // Agrega el nuevo servicio al array
            foreach($request->servicios as $value){
                // array_push($servicios, $request->servicios);
                array_push($servicios, $value);
            }

            // Actualiza la propiedad servicios
            $lab->servicios = $servicios;

            //Guardamos los cambios con el nuevo servicio agregado
            $lab->save();

            // Retornamos operacion exitosa
            return response()->json(['Servicio guardado con éxito'], 200);
        } else {
            return response()->json(['Laboratorio no fue encontrado'], 404);
        }
    }

    // Función para modificar un servicio
    public function modificarServicio(Request $request, $idregistrarlaboratio){

        // Validamos los valores necesarios
        $request->validate([
            'servicio_id' => 'required|integer',
            'servicio' => 'required|string',
        ]);

        // Verificamos que el id del laboratorio sea numerico
        $idregistrarlaboratio = (int) $idregistrarlaboratio;

        if($idregistrarlaboratio === 0) { // Cuando se parsea una cadena a string siempre da 0
            return response()->json(['message' => 'Tipo de dato no válido']);
        }

        // Obtenemos el laboratorio en el que ingresar el nuevo servicio
        $lab = RegistroLaboratorio::where('registro_id', '=', $idregistrarlaboratio)->first();

        // Verificamos que exista el laboratorio
        if($lab) {

            $servicios = $lab->servicios ?? [];

            // Modificamos el valor deseado
            // $lab->servicios[$request->servicio_id] = $request->servicio;
            $servicios[$request->servicio_id] = $request->servicio;


            $lab->servicios = $servicios;

            //Guardamos los cambios con el nuevo servicio agregado
            $lab->save();

            // Retornamos operacion exitosa
            return response()->json(['Servicio modificado con éxito'], 200);
        } else {
            return response()->json(['Laboratorio no fue encontrado'], 404);
        }
    }

    // Función para eliminar un servicio
    public function eliminarServicio(Request $request, $idregistrarlaboratio){

        // Validamos los valores necesarios
        $request->validate([
            'servicio_id' => 'required|integer',
        ]);

        // Verificamos que el id del laboratorio sea numerico
        $idregistrarlaboratio = (int) $idregistrarlaboratio;

        if($idregistrarlaboratio === 0) { // Cuando se parsea una cadena a string siempre da 0
            return response()->json(['message' => 'Tipo de dato no válido']);
        }

        // Obtenemos el laboratorio en el que ingresar el nuevo servicio
        $lab = RegistroLaboratorio::where('registro_id', '=', $idregistrarlaboratio)->first();

        // Verificamos que exista el laboratorio
        if($lab) {

            $servicios = $lab->servicios ?? [];

            // Eliminamos el valor deseado
            array_splice($servicios, $request->servicio_id, 1);


            $lab->servicios = $servicios;

            //Guardamos los cambios con el nuevo servicio agregado
            $lab->save();

            // Retornamos operacion exitosa
            return response()->json(['Servicio eliminado con éxito'], 200);
        } else {
            // Retornamos que no existe el laboratorio
            return response()->json(['Laboratorio no fue encontrado'], 404);
        }
    }
}
