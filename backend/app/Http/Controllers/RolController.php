<?php

namespace App\Http\Controllers;

use App\Models\Rol;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class RolController extends Controller
{

    /**
     * Mostrar roles
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Rol::where('estado', 'true')->orderBy('rol_id', 'asc')->get();
        return response()->json(['roles' => $roles]);
    }

    /**
     * Registrar rol
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'nombre' => 'required|max:45'
          ];
    
          $validator = Validator::make($request->all(), $rules);
    
          if($validator->fails()){
            return response()->json(['message'=>'Error de validación', 'errors'=>$validator->errors()], 422);
          }
    
          try{
            #if(!empty(trim($request->get('nombre')))){
              $rol = new Rol;
              $rol->nombre = $request->get('nombre');
              #$rol->nombre = $request->get('nombre');
              $rol->save();
              return response()->json(['status' => 'Creado Correctamente']); 
            #}
            #else{
            #    return response()->json(['message' => 'El campo nombre es requerido']);
            #}
          }catch (QueryException $e){
              return response()->json(['message'=> 'Ocurrio un error', 'error'=>$e]);
          }
    }

    /**
     * Mostrar rol por id
     *
     * @param  int $idrol
     * @return \Illuminate\Http\Response
     */
    public function show($idrol)
    {
        $idrol = (int) $idrol;
        //Verificar que el idrol es de tipo integer
        if($idrol === 0) {
            return response()->json(['message' => 'Tipo de dato no válido']);
        }

        $rol = Rol::where('rol_id','=',$idrol)->first();
        if($rol){
            return response()->json(['rol' => $rol]);
        } else {
            return response()->json(['message' => 'Rol no encontrado'], 404);
        }
    }

    /**
     * Actualizar rol  por id
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int $idrol
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $idrol)
    {
        $idrol = (int) $idrol;
        //Verificar que el idrol es de tipo integer
        if($idrol === 0) {
            return response()->json(['message' => 'Tipo de dato no válido']);
        }

        $rules = [
          'nombre' => 'required|max:45'
        ];

        $validator = Validator::make($request->all(), $rules);

        if($validator->fails()){
          return response()->json(['message'=>'Error de validació', 'errors'=>$validator->errors()], 422);
        }

        $rol = Rol::where('rol_id','=',$idrol)->first();
        if($rol && $rol->estado){
            try{
                if(!empty(trim($request->get('nombre')))){
                    $rol->nombre = $request->get('nombre');
                #if(!empty(trim($request->get('nombre')))){
                #    $rol->nombre = $request->get('nombre');
                    $rol->update();
                    return response()->json(['status' => 'Actualizado Correctamente']);
                }else{
                    return response()->json(['message' => 'El campo tiporol es requerido']);
                }
            } catch (QueryException $e){
                return response()->json(['message'=> 'El rol ya esta registrado']);
            }
        } else {
            return response()->json(['message' => 'Rol no encontrado'], 404);
        }
    }

    /**
     * Eliminar rol por id
     *
     * @param  int $idrol
     * @return \Illuminate\Http\Response
     */
    public function delete($idrol)
    {
        $idrol = (int) $idrol;
        //Verificar que el idrol es de tipo integer
        if($idrol === 0) {
            return response()->json(['message' => 'Tipo de dato no válido']);
        }

        $rol = Rol::where('rol_id','=',$idrol)->first();
        if($rol && $rol->estado) {
            $rol->estado = false;
            $rol->save();
            return response()->json(['status' => 'Eliminado Correctamente']); 
        } else {
            return response()->json(['message' => 'Rol no encontrado'], 404);
        }
    }
}
