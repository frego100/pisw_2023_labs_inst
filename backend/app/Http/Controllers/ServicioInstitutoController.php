<?php

namespace App\Http\Controllers;

use App\Models\ConvenioInstituto;
use App\Models\ServicioInstituto;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class ServicioInstitutoController extends Controller
{
    //

    public function index($instituto_id)
    {
        try {
            // Hacemos la consulta a la base de datos mediante el id de instituto
            $servicios = $instituto_id
                ? ServicioInstituto::where('instituto_id', $instituto_id)->get()
                : ServicioInstituto::all();

            // Retornamos en formato Json la respuesta
            return response()->json(['servicios' => $servicios]);
        } catch (\Exception $e) {
            // Manejar la excepción y retornar una respuesta de error
            return response()->json(['error' => 'Error en la consulta: ' . $e->getMessage()], 500);
        }
    }
    public function store(Request $request)
    {   //Validamos las entradas de request
        try {
            $request->validate([
                'instituto_id' => 'required',
                'servicio' => 'required',
                'materiales' => 'required',
                'mano_obra' => 'required',
                'ci_mano_obra' => 'required',
                'ci_deprec_equi' => 'required',
                'ci_deprec_edif' => 'required',
                'ci_util_limp' => 'required',
                'ci_util_aseo' => 'required',
                'ci_mantto' => 'required',
                'ci_servicios' => 'required',
            ]);
            //Ingresamos los valores mediante el modelo
            $servicio = new ServicioInstituto;
            $servicio->instituto_id = $request->instituto_id;
            $servicio->servicio = $request->servicio;
            $servicio->materiales = $request->materiales;
            $servicio->mano_obra = $request->mano_obra;
            $servicio->ci_mano_obra = $request->ci_mano_obra;
            $servicio->ci_deprec_equi = $request->ci_deprec_equi;
            $servicio->ci_deprec_edif = $request->ci_deprec_edif;
            $servicio->ci_util_limp = $request->ci_util_limp;
            $servicio->ci_util_aseo = $request->ci_util_aseo;
            $servicio->ci_mantto = $request->ci_mantto;
            $servicio->ci_servicios = $request->ci_servicios;

            //Guardamos los valores
            $servicio->save();
            //Enviamos mensaje de exito con el codigo 201
            return response()->json(['servicio' => $servicio, 'message' => 'Servicio creado correctamente'], 201);
        } catch (\Exception $e) {
            //Envia mensaje de error con el codigo 500
            return response()->json(['error' => 'Error al guardar el servicio: ' . $e->getMessage()], 500);
        }
    }

    public function show($id_servicio)
    {
        $id_servicio = (int) $id_servicio;
        //Verificar que el idinstituto es de tipo integer
        if($id_servicio === 0) {
            return response()->json(['message' => 'Tipo de dato no válido']);
        }
        // Obtener la función por ID con su relación con el instituto
        $servicio = ServicioInstituto::where('servicio_id', '=', $id_servicio)->first();

        // Verificar si la función existe
        if($servicio){
            return response()->json(['servicio' => $servicio]);
        } else {
            return response()->json(['message' => 'Servicio no encontrado'], 404);
        }
    }

    public function update(Request $request, $id_servicio)
    {
        $id_servicio = (int) $id_servicio;
        //Verificar que el idinstituto es de tipo integer
        if ($id_servicio === 0) {
            return response()->json(['message' => 'Tipo de dato no válido']);
        }
        $servicio = ServicioInstituto::where('servicio_id', '=', $id_servicio)->first();
        if ($servicio) {
            try {
                //Se guardan los valores
                $servicio->instituto_id = $request->instituto_id;
                $servicio->servicio = $request->servicio;
                $servicio->materiales = $request->materiales;
                $servicio->mano_obra = $request->mano_obra;
                $servicio->ci_mano_obra = $request->ci_mano_obra;
                $servicio->ci_deprec_equi = $request->ci_deprec_equi;
                $servicio->ci_deprec_edif = $request->ci_deprec_edif;
                $servicio->ci_util_limp = $request->ci_util_limp;
                $servicio->ci_util_aseo = $request->ci_util_aseo;
                $servicio->ci_mantto = $request->ci_mantto;
                $servicio->ci_servicios = $request->ci_servicios;
                $servicio->update();
                //Se envia mensaje de exito
                return response()->json(['status' => 'Actualizado Correctamente']);
            } catch (QueryException $e) {
                //Se envia mensaje de error
                return response()->json(['message' => 'Error al actualizar servicio']);
            }
        } else {
            return response()->json(['message' => 'Servicio no encontrado'], 404);
        }
    }

    public function delete($id_servicio)
    {
        $id_servicio = (int) $id_servicio;
        //Verificar que el idinstituto es de tipo integer
        if ($id_servicio === 0) {
            return response()->json(['message' => 'Tipo de dato no válido']);
        }
        //Buscamos el servicio
        $servicio = ServicioInstituto::where('servicio_id', '=', $id_servicio)->first();
        if ($servicio && $servicio->estado) {
            try {
                //Cambiamos el estado a false
                $servicio->estado = false;
                $servicio->save();
                //Enviamos mensaje de exito
                return response()->json(['status' => 'Eliminado Correctamente']);
            } catch (QueryException $e) {
                //Enviamos mensaje de error
                return response()->json(['message' => 'Error al eliminar servicio']);
            }
        } else {
            return response()->json(['message' => 'Servicio no encontrado'], 404);
        }
    }

}
