<?php

namespace App\Http\Controllers;

use App\Models\Solicitud;
use App\Models\Equipo;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\QueryException;

class SolicitudController extends Controller
{
    // Método para listar las solicitudes
    public function index(){
        // Muestra la informacion de la solicitud incluyendo informacion del Equipo, el Usuario y el Laboratorio
        $solicitudes = Solicitud::orderBy('solicitud_id', 'asc')->with('equipo')->with('usuario')->with('registro_laboratorio')->with('registro_laboratorio.laboratorio')->get();

        // Retornamos una respuesta
        return response()->json(['solicitudes'=>$solicitudes]);
    }

    public function store(Request $request){

        $idregistro_laboratorio = Equipo::where('equipo_id', $request->equipo_id)->first();
        try {
            $solicitud = new Solicitud;
            $solicitud->usuario_id = $request->usuario_id;
            $solicitud->equipo_id = $request->equipo_id;
            $solicitud->laboratorio_id = $idregistro_laboratorio->registro_id;
            $solicitud->fecha_solicitud = $request->fecha_solicitud;
            $solicitud->detalle = $request->detalle;
            $solicitud->oficio = $request->oficio;
            $solicitud->etapa = 'INICIO';

            $solicitud->save();

            return response()->json(['status'=> 'Creado correctamente'], 200);
        } catch (QueryException $e) {
            return response()->json(['message'=> 'Ocurrio un error', 'error'=>$e]);
        }
    }

    public function show($idsolicitud){
        $idsolicitud = (int) $idsolicitud;
        //Verificar que el idproyecto es de tipo integer
        if($idsolicitud === 0) {
            return response()->json(['message' => 'Tipo de dato no válido']);
        }

        $solicitud = Solicitud::where('solicitud_id', $idsolicitud)->with('equipo')->with('usuario')->first();

        if($solicitud){
            return response()->json(['solicitud' => $solicitud]);
        } else {
            return response()->json(['message'=>'Solicitud no encontrada'], 404);
        }
    }

    public function update(Request $request, $idsolicitud) {
        $idsolicitud = (int) $idsolicitud;
        //Verificar que el idproyecto es de tipo integer
        if($idsolicitud === 0) {
            return response()->json(['message' => 'Tipo de dato no válido']);
        }

        try {
            $solicitud = Solicitud::where('solicitud_id', $idsolicitud)->first();
            $solicitud->fecha_solicitud = $request->fecha_solicitud;
            $solicitud->detalle = $request->detalle;
            $solicitud->oficio = $request->oficio;
            $solicitud->etapa = $request->etapa;

            $solicitud->save();
            return response()->json(['status'=>'Actualizado correctamente'], 200);
        } catch (QueryException $e) {
            return response()->json(['message'=> 'Ocurrio un error', 'error'=>$e]);
        }
    }

    public function delete($idsolicitud)
    {
        //
        $solicitud =  Solicitud::all()->where('solicitud_id', '=', $idsolicitud)->first();

        if (is_null($solicitud)) {
            return response()->json('No se pudo realizar correctamente la operación', 404);
        }

        $solicitud->delete();
        return response()->json(['status' => 'Eliminado Correctamente']);
    }
}
