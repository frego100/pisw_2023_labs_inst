<?php

namespace App\Http\Controllers;

use App\Models\Instituto;
use App\Models\User;
use App\Models\OperadorLaboratorio;
use App\Models\OperadorInstituto;
use App\Models\RegistroLaboratorio;
use App\Models\Laboratorio;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Redis;

class UserController extends Controller
{

  /**
   * Función para mostrar listar los usuarios
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function __construct()
  {
    $this->middleware('verificarRol:1', ['only' => ['store', 'update', 'delete']]);
  }

  public function index()
  {
    //Mostrar los dat
    // $usuarios=User::orderBy('id', 'asc')->get();
    $usuarios = User::orderBy('usuario_id', 'asc')->get();
    return response()->json(['usuarios' => $usuarios]);
  }

  /**
   * Función para mostrar listar coordinadores
   * @return \Illuminate\Http\Response
   */
  public function getCoordinadores()
  {
    $usuarios = User::where('rol_id', 2)
      ->orderBy('usuario_id', 'asc')
      ->get();

    return response()->json(['coordinadores' => $usuarios], 200);
  }

  /**
   * Función para mostrar listar operadores
   * @return \Illuminate\Http\Response
   */
  public function getOperadores()
  {
    $usuarios = User::where('rol_id', 3)
      ->orderBy('usuario_id', 'asc')
      ->get();

    return response()->json(['operadores' => $usuarios], 200);
  }

  /**
   * Función para guardar usuario
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $rules = [
      'documento' => 'required|unique:users,dni|digits:8',
      'nombreusuario' => 'required|string|max:45',
      'apellidopa' => 'required|string|max:45',
      'apellidoma' => 'required|string|max:45',
      'direccionusuario' => 'required|string|max:45',
      'telefonousuario' => 'required|string|digits:9',
      'email' => 'required|email|unique:users,correo',
      'password' => 'required|string|min:6',
      'idrol' => 'required|exists:roles,rol_id',
      'estado' => 'required|boolean',
    ];

    $validator = Validator::make($request->all(), $rules);

    if ($validator->fails()) {
      return response()->json(['message' => 'Error de validacion', 'errors' => $validator->errors()], 422);
    }

    try {
      $usuario = new User;
      $usuario->dni = $request->get('documento');
      $usuario->nombre = $request->get('nombreusuario');
      $usuario->apellido_paterno = $request->get('apellidopa');
      $usuario->apellido_materno = $request->get('apellidoma');
      $usuario->direccion = $request->get('direccionusuario');
      $usuario->telefono = $request->get('telefonousuario');
      $usuario->correo = $request->get('email');
      $usuario->contrasena = Hash::make($request->get('password'));
      $usuario->rol_id = $request->get('idrol');
      $usuario->estado = $request->get('estado');
      $usuario->save();
      return response()->json(['status' => 'Creado Correctamente'], 201);
    } catch (QueryException $e) {
      return response()->json(['message' => $e]);
    }
  }

  /**
   * Función para obtener un usuario por su id
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($idusuario)
  {
    $idusuario = (int) $idusuario;
    //Verificar que el idusuario es de tipo integer
    if ($idusuario === 0) {
      return response()->json(['message' => 'Tipo de dato no válido']);
    }

    $usuario = User::with('rol')->where('usuario_id', '=', $idusuario)->first();
    if ($usuario && $usuario->estado) {
      $usuario->makeHidden('rol_id');
      return response()->json(['usuario' => $usuario]);
    } else {
      return response()->json(['message' => 'Valor no existente en la base de datos'], 404);
    }
  }


  /**
   * Función para actualizar un usuario
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $idusuario)
  {

    $idusuario = (int) $idusuario;
    //Verificar que el idusuario es de tipo integer
    if ($idusuario === 0) {
      return response()->json(['message' => 'Tipo de dato no válido'], 400);
    }

    $rules = [
      'nombreusuario' => 'required|string|max:45',
      'apellidopa' => 'required|string|max:45',
      'apellidoma' => 'required|string|max:45',
      'direccionusuario' => 'required|string|max:45',
      'telefonousuario' => 'required|string|digits:9',
      'password' => 'required|string|min:6',
      'idrol' => 'required|exists:roles,rol_id',
      'estado' => 'required|boolean',
    ];

    $validator = Validator::make($request->all(), $rules);

    if ($validator->fails()) {
      return response()->json(['message' => 'Error de validacion', 'errors' => $validator->errors()], 422);
    }

    if (!(substr($request->email, -12) === "@unsa.edu.pe")) {
      return response()->json(['message' => 'Correo no válido'], 400);
    }

    //Actualizamos el registro
    $usuario = User::where('usuario_id', '=', $idusuario)->first();
    if ($usuario && $usuario->estado) {
      $usuario->nombre = $request->nombreusuario;
      $usuario->apellido_paterno = $request->apellidopa;
      $usuario->apellido_materno = $request->apellidoma;
      $usuario->direccion = $request->direccionusuario;
      $usuario->telefono = $request->telefonousuario;
      $usuario->dni = $request->documento;
      $usuario->contrasena = Hash::make($request->get('password'));;
      $usuario->rol_id = $request->idrol;
      $usuario->estado = $request->estado;
      $usuario->update();
      return response()->json(['status' => 'Actualizado Correctamente'], 200);
    } else {
      return response()->json(['message' => 'Usuario no existe'], 404);
    }
  }

  /**
   * Función para la eliminación lógica de un usuario
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function delete($idusuario)
  {
    $idusuario = (int) $idusuario;
    //Verificar que el idusuario es de tipo integer
    if ($idusuario === 0) {
      return response()->json(['message' => 'Tipo de dato no válido']);
    }

    $usuario = User::where('usuario_id', '=', $idusuario)->first();

    if ($usuario && $usuario->estado) {
      $usuario->estado = false;
      $usuario->save();
      return response()->json(['status' => 'Eliminado correctamente']);
    }
    return response()->json(['message' => 'El usuario no existe'], 404);
  }

  /**
   * Función para obtener un usuario por su nombre de usuario
   */
  public function getAllByNombreUsuarios()
  {
    return response()->json(['usuarios' => []], 404);
  }

  /**
   * Función para obtener un usuario por su nombre de usuario
   *
   * @param string $nombreusuario
   */
  public function getAllByNombreUsuario($nombreusuario)
  {

    if (isset($nombreusuario)) {
      $usuarios = User::with('rol')->where('nombre', 'LIKE', '%' . $nombreusuario . '%')
        ->get();
      return response()->json(['usuarios' => $usuarios]);
    } else {
      return response()->json(['usuarios' => []], 404);
    }
  }

  /**
   * Función para obtener un usuario por su email de usuario
   */
  public function getAllByEmails()
  {
    return response()->json(['usuarios' => []], 404);
  }

  /**
   * Función para obtener un usuario por su email de usuario
   *
   * @param string $email
   */
  public function getAllByEmail($email)
  {
    $usuarios = User::with('rol')->where('correo', 'LIKE', '%' . $email . '%')
      ->where('estado', true)
      ->get();
    return response()->json(['usuarios' => $usuarios]);
  }

  /**
   * Función para realizar busquedas
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function busqueda(Request $request)
  {
    $tipo = $request->tipo;
    $textoBusqueda = $request->textoBusqueda;
    switch ($tipo) {
        // nombreusuario
      case 'nombreUsuario':
        return User::with('rol')->where('nombreusuario', 'LIKE', '%' . $textoBusqueda . '%')->get();
        break;
        //rol
      case 'rol':
        return User::with('rol')->whereHas('rol', function ($query) use ($textoBusqueda) {
          $query->where('tiporol', 'LIKE', '%' . $textoBusqueda . '%');
        })->get();
        break;
        //todos
      case 'nombreUsuarioAndRol':
        return User::with('rol')
          ->orWhere('nombreusuario', 'LIKE', '%' . $textoBusqueda . '%')
          ->orWhereHas('rol', function ($query) use ($textoBusqueda) {
            $query->where('tiporol', 'LIKE', '%' . $textoBusqueda . '%');
          })
          ->get();
        break;
      default:
        // Si el campo de búsqueda no es válido, retorna un JSON vacío
        return response()->json([], 200);
    }
  }
  public function getUser(Request $request)
  {
    $user = $request->user();
    return $user;
  }


  //Metodo para listar operadores
  public function listarOperadores()
  {
    $operadores = User::where('estado', true)->where('rol_id', 3)->get();

    return response()->json(['operadores' => $operadores], 200);
  }

  //Metodo para ver los laboratorios en los que esta registrado
  //PENDIENTE CON CAMBIO EN LA DB PARA QUE PERMITA GUARDAR ARRAY
  public function verMisLaboratoriosOperador($idoperador)
  {

    $idoperador = (int) $idoperador;

    if ($idoperador === 0) {
      return response()->json(['message' => 'Tipo de dato no válido'], 400);
    } else


      $registro = OperadorLaboratorio::where('estado', true)->where('operador_id', $idoperador)->pluck('registro_id')
        ->toArray();

    $registroid = RegistroLaboratorio::where('estado', true)->wherein('registro_id', $registro)->select('laboratorio_id', 'coordinador_id')
      ->get();

    $res = [];
    foreach ($registroid as $regis) {
      $laboratorio = Laboratorio::where('laboratorio_id', $regis->laboratorio_id)->first();
      $coordinador = User::where('usuario_id', $regis->coordinador_id)->first();
      $res[] = [
        'nombre_laboratorio' => $laboratorio ? $laboratorio->nombre : null,
        'nombre_coordinador' => $coordinador ? $coordinador->nombre : null,
      ];
    }
    return response()->json($res, 200);
  }

  public function verMisInstitutosOperador($idoperador)
  {

    $idoperador = (int) $idoperador;

    if ($idoperador === 0) {
      return response()->json(['message' => 'Tipo de dato no válido'], 400);
    } else


      $registro = Instituto::where('estado', true)->get()
        ->filter(function ($item) use ($idoperador) {
          $comiteDirectivoArray = explode(',', $item->comite_directivo);
          return in_array($idoperador, $comiteDirectivoArray);
        });

    $res = $registro->toArray();

    $nres = [];
    foreach ($res as $elemento) {
      $nombreUsuarioDirector = User::where('usuario_id', $elemento['usuario_director'])->value('nombre');

      $nuevoElemento = [
        'nombre' => $elemento['nombre'],
        'director' => $nombreUsuarioDirector
      ];

      $nres[] = $nuevoElemento;
    }


    /*
    $registroid = RegistroLaboratorio::where('estado', true)->wherein('registro_id', $registro)->select('laboratorio_id', 'coordinador_id')
      ->get();

    $res = [];
    foreach ($registroid as $regis) {
      $laboratorio = Laboratorio::where('laboratorio_id', $regis->laboratorio_id)->first();
      $coordinador = User::where('usuario_id', $regis->coordinador_id)->first();
      $res[] = [
        'nombre_laboratorio' => $laboratorio ? $laboratorio->nombre : null,
        'nombre_coordinador' => $coordinador ? $coordinador->nombre : null,
      ];
    }*/
    return response()->json(array_values($nres), 200);
  }

  //Metodo para crear nuevos operadores desde coordinador
  public function crearOperadorDesdeCoordinador(Request $request)
  {
    $rules = [
      'documento' => 'required|unique:users,dni|digits:8',
      'nombreusuario' => 'required|string|max:45',
      'apellidopa' => 'required|string|max:45',
      'apellidoma' => 'required|string|max:45',
      'direccionusuario' => 'required|string|max:45',
      'telefonousuario' => 'required|string|digits:9',
      'email' => 'required|email|unique:users,correo',
      'password' => 'required|string|min:6',
      'estado' => 'boolean',
      'categoria' => 'required|string',
      'regimen' => 'required|string'
    ];

    $validator = Validator::make($request->all(), $rules);

    if ($validator->fails()) {
      return response()->json(['message' => 'Error de validacion', 'errors' => $validator->errors()], 422);
    }

    try {
      $usuario = new User;
      $usuario->dni = $request->get('documento');
      $usuario->nombre = $request->get('nombreusuario');
      $usuario->apellido_paterno = $request->get('apellidopa');
      $usuario->apellido_materno = $request->get('apellidoma');
      $usuario->direccion = $request->get('direccionusuario');
      $usuario->telefono = $request->get('telefonousuario');
      $usuario->correo = $request->get('email');
      $usuario->contrasena = Hash::make($request->get('password'));
      $usuario->rol_id = 3;
      $usuario->estado = true;
      $usuario->categoria = $request->get('categoria');
      $usuario->regimen = $request->get('regimen');
      $usuario->save();
      return response()->json(['status' => 'Creado Correctamente']);
    } catch (QueryException $e) {
      return response()->json(['messages' => $e]);
    }
  }

  //borrar operador desde coordinador
  public function deleteOperadorFromCoordinador($idusuario)
  {
    $idusuario = (int) $idusuario;
    //Verificar que el idusuario es de tipo integer
    if ($idusuario === 0) {
      return response()->json(['message' => 'Tipo de dato no válido']);
    }

    $usuario = User::where('usuario_id', '=', $idusuario)->first();

    if ($usuario && $usuario->estado) {
      $usuario->estado = false;
      $usuario->save();
      return response()->json(['status' => 'Eliminado correctamente']);
    }
    return response()->json(['message' => 'El usuario no existe'], 404);
  }

  // METODOS PARA LOS DIRECTORES DE INSTITUTOS
  public function listarDirectores()
  {
    $directores = User::where('rol_id', '=', 4)->get();
    return response()->json(['directores' => $directores]);
  }

  public function completarInfoDirector(Request $request, $iddirector)
  {
    $iddirector = (int) $iddirector;
    //Verificar que el idinstituto es de tipo integer
    if ($iddirector === 0) {
      return response()->json(['message' => 'Tipo de dato no válido']);
    }

    try {
      $director = User::where('usuario_id', $iddirector)->first();

      $director->resumen = $request->resumen;
      $director->fecha_eleccion = $request->fecha_eleccion;
      $director->fecha_culminacion = $request->fecha_culminacion;

      $director->update();
      return response()->json(['status' => $request]);
    } catch (QueryException $e) {
      return response()->json(['message' => 'Ocurrio un error', 'error' => $e]);
    }
  }


  /**
   * Función para guardar Comite Directivo
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function storeComiteDirectivo(Request $request)
  {
    /* estableciendo las validaciones para cada atributo del objeto que estamos recibiendo */
    $rules = [
      'documento' => 'required|unique:users,dni|digits:8',
      'nombreusuario' => 'required|string|max:45',
      'apellidopa' => 'required|string|max:45',
      'apellidoma' => 'required|string|max:45',
      'direccionusuario' => 'required|string|max:45',
      'telefonousuario' => 'required|string|digits:9',
      'email' => 'required|email|unique:users,correo',
      'password' => 'required|string|min:6',
      'categoria' => 'required|string',
      'regimen' => 'required|string'
    ];

    /* obteniendo el resultado de las validaciones aplicadas a todos los parametros del objeto */
    $validator = Validator::make($request->all(), $rules);

    /* comprobando si alguna validación no se cumplió */
    if ($validator->fails()) {
      return response()->json(['message' => 'Error de validacion', 'errors' => $validator->errors()], 422);
    }

    try {
      /* creando el objeto */
      $usuario = new User;
      $usuario->dni = $request->get('documento');
      $usuario->nombre = $request->get('nombreusuario');
      $usuario->apellido_paterno = $request->get('apellidopa');
      $usuario->apellido_materno = $request->get('apellidoma');
      $usuario->direccion = $request->get('direccionusuario');
      $usuario->telefono = $request->get('telefonousuario');
      $usuario->correo = $request->get('email');
      $usuario->contrasena = Hash::make($request->get('password'));
      $usuario->rol_id = 5;
      $usuario->estado = true;
      $usuario->categoria = $request->get('categoria');
      $usuario->regimen = $request->get('regimen');
      /* guardando el objeto */
      $usuario->save();
      return response()->json(['status' => 'Creado Correctamente'], 201);
    } catch (QueryException $e) {
      return response()->json(['message' => $e]);
    }
  }

  /**
   * Función para mostrar listar comite directivo
   * @return \Illuminate\Http\Response
   */
  public function indexComiteDirectivo()
  {
    /* listar todos los usuarios con el rol de comite directivo */
    $comteDirectivo = User::whereHas('rol', function ($query) {
      $query->where('nombre', 'LIKE', '%directivo%');
    })->get();
    return response()->json(['comiteDirectivo' => $comteDirectivo]);
  }

  /**
   * Función para obtener un usuario por su id
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function showComiteDirectivo($idComteDirectivo)
  {
    $idComteDirectivo = (int) $idComteDirectivo;
    //Verificar que el idusuario es de tipo integer
    if ($idComteDirectivo === 0) {
      return response()->json(['message' => 'Tipo de dato no válido']);
    }
    /* buscando al comite directivo por su id */
    $comiteDirectivo = User::with('rol')->where('usuario_id', '=', $idComteDirectivo)->first();
    /* validando si el comite directivo existe y se encuentra activo */
    if ($comiteDirectivo && $comiteDirectivo->estado) {
      $comiteDirectivo->makeHidden('rol_id');
      return response()->json(['comite directivo' => $comiteDirectivo]);
    } else {
      return response()->json(['message' => 'Comite Directivo no existe'], 404);
    }
  }

  /**
   * Función para actualizar un usuario
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function updateComiteDirectivo(Request $request, $idComteDirectivo)
  {

    $idComteDirectivo = (int) $idComteDirectivo;
    //Verificar que el idusuario es de tipo integer
    if ($idComteDirectivo === 0) {
      return response()->json(['message' => 'Tipo de dato no válido'], 400);
    }

    /* estableciendo las validaciones para cada atributo del objeto que estamos recibiendo */
    $rules = [
      'nombreusuario' => 'required|string|max:45',
      'apellidopa' => 'required|string|max:45',
      'apellidoma' => 'required|string|max:45',
      'direccionusuario' => 'required|string|max:45',
      'telefonousuario' => 'required|string|digits:9',
      'password' => 'required|string|min:6',
      'idrol' => 'required|exists:roles,rol_id',
      'categoria' => 'required|string',
      'regimen' => 'required|string'
    ];

    /* obteniendo el resultado de las validaciones aplicadas a todos los parametros del objeto */
    $validator = Validator::make($request->all(), $rules);

    /* comprobando si alguna validación no se cumplió */
    if ($validator->fails()) {
      return response()->json(['message' => 'Error de validacion', 'errors' => $validator->errors()], 422);
    }

    /* buscando al comite directivo por su id */
    $usuario = User::where('usuario_id', '=', $idComteDirectivo)->first();
    /* validando si el comite directivo existe y se encuentra activo */
    if ($usuario && $usuario->estado) {
      /* actualizando los datos del comite directivo */
      $usuario->nombre = $request->get('nombreusuario');
      $usuario->apellido_paterno = $request->get('apellidopa');
      $usuario->apellido_materno = $request->get('apellidoma');
      $usuario->direccion = $request->get('direccionusuario');
      $usuario->telefono = $request->get('telefonousuario');
      $usuario->contrasena = Hash::make($request->get('password'));
      $usuario->rol_id = $request->get('idrol'); // PUEDE SER 5, COMITE DIRECTIVO
      $usuario->estado = true;
      $usuario->categoria = $request->get('categoria');
      $usuario->regimen = $request->get('regimen');
      /* actualizar los datos del comite directivo */
      $usuario->update();
      return response()->json(['status' => 'Actualizado Correctamente'], 200);
    } else {
      return response()->json(['message' => 'Comite Directivo no existe'], 404);
    }
  }

  /**
   * Función para la eliminación de comite directivo
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function deleteComiteDiretivo($idComteDirectivo)
  {
    $idComteDirectivo = (int) $idComteDirectivo;
    //Verificar que el idusuario es de tipo integer
    if ($idComteDirectivo === 0) {
      return response()->json(['message' => 'Tipo de dato no válido']);
    }
    /* buscando al comite directivo por su id */
    $comiteDirectivo = User::where('usuario_id', '=', $idComteDirectivo)->first();

    /* validando si el comite directivo existe y se encuentra activo */
    if ($comiteDirectivo && $comiteDirectivo->estado) {
      /* cambiando el estado del comite directivo a false */
      $comiteDirectivo->estado = false;
      /* guardando el cambio */
      $comiteDirectivo->save();
      return response()->json(['status' => 'Eliminado correctamente']);
    }
    return response()->json(['message' => 'Comite Directivo no existe'], 404);
  }
}
