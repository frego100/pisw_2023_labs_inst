<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Models\Rol;

class AdminOrDirectorMiddelware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $user = $request->user();
        $rol = Rol::where('rol_id','=',$user->rol_id)->first();

        if (strtolower($rol->nombre) !== 'administrador' && strtolower($rol->nombre) !== 'director' && strtolower($rol->nombre) !== 'admininstitutos') {
            return response()->json(['message' => 'El usuario no tiene permisos para realizar esta acción'], 401);
        }

        return $next($request);
    }
}
