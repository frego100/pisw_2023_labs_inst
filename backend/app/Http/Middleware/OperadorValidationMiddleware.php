<?php

namespace App\Http\Middleware;

use App\Models\Rol;
use Closure;
use Illuminate\Http\Request;

class OperadorValidationMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {

        $user = $request->user();
        $rol = Rol::where('rol_id', '=', $user->rol_id)->first();

        // Verifica si el usuario en sesión existe y si tiene un rol asignado
        // if (!$user || !$user->idrol) {
        //     return response()->json(['message' => 'El usuario en sesión no tiene un rol asignado'], 403);
        // }

        if ($rol->rol_id !== 3 && $rol->rol_id !== 5 &&  strtolower($rol->nombre) !== 'adminlaboratorios') {
            return response()->json(['message' => 'El usuario en sesión no puede realizar esta acción'], 403);
        }

        return $next($request);
    }
}
