<?php

namespace App\Http\Requests\Poi;

use App\Models\Poi;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;
use App\Http\Responses\ApiResponse;

class PoiStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules() : array
    {
        return [
            'instituto_id' => 'required|integer|exists:institutos,instituto_id',
            'year' => [
                'required',
                'integer',
                function ($attribute, $year, $fail) {
                    $existingPoi = Poi::where('instituto_id', $this->input('instituto_id'))
                        ->where('year', $year)
                        ->first();
    
                    if ($existingPoi) {
                        $fail('Ya existe un POI para este año en este instituto.');
                    }
                },
            ],
            'document_uri'=>'required|file'
        ];
    }

    public function messages(){
        return [
            // 'instituto_id.required'=>'El campo instituto_id es requerido',
            'instituto_id.integer'=>'El campo instituto_id tiene que ser un valor numerico',
            'instituto_id.exists'=>'El campo instituto_id seleccionado no es válido',

            'year.required'=>'El campo year es requerido',
            'year.integer'=>'El campo year tiene que ser un valor numerico',

            'document_uri.required'=>'El campo document_uri es requerido',
            'document_uri.string'=>'El campo document_uri tiene que ser una cadena de texto'
        ];
    }

    protected function failedValidation(Validator $validator){
        $errors = $validator->errors()->toArray();
        throw new ValidationException($validator, ApiResponse::error('Error de Validación', 422, $errors));
    }
}
