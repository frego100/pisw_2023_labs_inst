<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Equipo extends Model
{
    use HasFactory;

    
    protected $table = 'equipos'; // Nombre de la tabla en la base de datos

    protected $primaryKey = 'equipo_id'; // Clave primaria personalizada

    protected $fillable = [
      'registro_id',
      'nombre_imagen',
      'imagen_equipo',
      'nombre',
      'marca_modelo',
      'fecha_adquisicion',
      'proveedor',
      'contacto_proveedor',
      'codigo_patrimonio',
      'accesorio',
      'insumos',
      'description',
      'estado',
    ];
    
    protected $appends = ['image_url'];

    public function getImageUrlAttribute()
    {
        // return asset($this->imagen_equipo);
        return asset('storage/' . $this->imagen_equipo);
    }
}
