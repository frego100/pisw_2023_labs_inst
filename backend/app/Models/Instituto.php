<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Instituto extends Model
{
    use HasFactory;
    protected $table = 'institutos'; // Nombre de la tabla en la base de datos

    protected $primaryKey = 'instituto_id'; // Clave primaria personalizada

    protected $fillable = [
        'instituto_id',
        'nombre',
        'mision',
        'vision',
        'historia',
        'usuario_director',
        'comite_directivo',
        'contacto',
        'nombre_imagen',
        'imagen_instituto',
        'url_instituto',
        'url_facebook',
        'estado',
    ];

    protected $casts = [
        'comite_directivo' => 'array', //array comite directivo
    ];

    protected $appends = ['image_url'];

    public function director(){
        return $this->hasOne(User::class, 'usuario_id', 'usuario_director');
    }

    public function getImageUrlAttribute()
    {
        // return asset($this->imagen_equipo);
        return asset('storage/' . $this->imagen_instituto);
    }

    public function pois(){
        return $this->hasMany(Poi::class, 'instituto_id', 'instituto_id');
    }

}
