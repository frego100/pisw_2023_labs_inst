<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OperadorInstituto extends Model
{
    use HasFactory;

    protected $table = 'operadores_institutos';
    protected $primaryKey = 'asignar_id';
    // Define las propiedades fillable para permitir la asignación en masa
    protected $fillable = [
        'operador_id',
        'instituto_id',
        'estado'
    ];

    public function operador(){
        return $this->hasOne(User::class, 'usuario_id', 'operador_id');
    }
}
