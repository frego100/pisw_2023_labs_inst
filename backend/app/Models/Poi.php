<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Poi extends Model
{
    use HasFactory;

    protected $fillable = [
        'instituto_id',
        'year',
        'document_uri',
        'nombre_poi',
    ];  

    public function instituto(){
        return $this->belongsTo(Instituto::class, 'instituto_id', 'instituto_id');
    }
}
