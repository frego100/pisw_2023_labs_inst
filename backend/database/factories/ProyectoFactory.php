<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Proyecto>
 */
class ProyectoFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            //Factories para el caso de prueba de la gestion de proyectos por operador
            'nombre_proyecto' => $this->faker->sentence,
            'investigador_principal' => $this->faker->name,
            'coinvestigadores' => $this->faker->name . ', ' . $this->faker->name,
            //'imagen_equipos_utilizados' => 'equipos.jpg',
            //'imagen_desarrollo_proyecto' => 'desarrollo.jpg',
            'doi' => $this->faker->word,
            'resumen' => $this->faker->paragraph,
            'iba' => $this->faker->word,
            'estado' => 1, // Puedes configurar esto según tus necesidades.
            'imagen_referencial' => 'images/proyecto.jpg',
        ];
    }
}
