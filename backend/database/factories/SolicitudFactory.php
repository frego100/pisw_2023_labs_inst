<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Solicitud>
 */
class SolicitudFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'usuario_id' => 1,
            'equipo_id' => 1,
            'fecha_solicitud' => $this->faker->date(),
            'detalle' => $this->faker->text(),
            'oficio' => $this->faker->text(10),
            'etapa' => 'Inicio', // Valor por defecto para 'etapa'
            'estado' => true,
            //
        ];
    }
}
