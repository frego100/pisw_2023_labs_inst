<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('equipos', function (Blueprint $table) {
            $table->id('equipo_id');
            $table->unsignedInteger('registro_id');
            $table->string('nombre_imagen')->nullable();
            $table->string('imagen_equipo')->nullable();
            $table->string('nombre', 255);
            $table->string('marca_modelo', 255);
            $table->datetime('fecha_adquisicion');
            $table->string('proveedor', 100);
            $table->string('contacto_proveedor', 100);
            $table->string('codigo_patrimonio', 50);
            $table->string('accesorio', 255);
            $table->text('insumos');
            $table->text('descripcion');
            $table->boolean('estado')->default(true);
            $table->timestamps();

            $table->foreign('registro_id')->references('registro_id')->on('registro_laboratorios')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('equipos', function (Blueprint $table) {
        $table->dropForeign(['registro_id']);
      });
      Schema::dropIfExists('equipos');
    }
};
