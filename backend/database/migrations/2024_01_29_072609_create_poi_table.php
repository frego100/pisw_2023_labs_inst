<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('poi', function (Blueprint $table) {
            $table->id('poi_id');
            $table->unsignedInteger('instituto_id');
            $table->string('año', 4);
            $table->string('nombre_poi')->nullable();
            $table->string('archivo_poi')->nullable();
            $table->boolean('estado')->default(true);
            $table->timestamps();

            $table->foreign('instituto_id')->references('instituto_id')->on('institutos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('poi', function (Blueprint $table) {
            $table->dropForeign(['instituto_id']);
          });
        Schema::dropIfExists('poi');
    }
};
