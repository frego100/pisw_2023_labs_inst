<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class DisciplinasTableSeeder extends Seeder
{
    /**
     * Seeder para llenar la tabla Disciplinas en la base de datos con los campos correspondientes a cada disciplina
     *
     * @return void
     */
    public function run()
    {
        DB::table('disciplinas')->insert([
            'nombre' => 'Arte; educación y cultura',
            'estado' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('disciplinas')->insert([
            'nombre' => 'Biodiversidad; ecología y convervación',
            'estado' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('disciplinas')->insert([
            'nombre' => 'Biofertilizantes',
            'estado' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('disciplinas')->insert([
            'nombre' => 'Biología molecular y celular',
            'estado' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('disciplinas')->insert([
            'nombre' => 'Bioquímica y genética',
            'estado' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('disciplinas')->insert([
            'nombre' => 'Biorremediación',
            'estado' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('disciplinas')->insert([
            'nombre' => 'Calidad ambiental y salud humana',
            'estado' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('disciplinas')->insert([
            'nombre' => 'Computación gráfica e imágenes',
            'estado' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('disciplinas')->insert([
            'nombre' => 'Comunicación política y medios digitales',
            'estado' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('disciplinas')->insert([
            'nombre' => 'Corrupción y reforma del estado',
            'estado' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('disciplinas')->insert([
            'nombre' => 'Democracia; ciudadanía; derechos humanos; desarrollo y justicia',
            'estado' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('disciplinas')->insert([
            'nombre' => 'Desarrollo y crecimiento económico',
            'estado' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('disciplinas')->insert([
            'nombre' => 'Diversificación de la oferta turística: Potencial turístico; producto y experiencias turísticas',
            'estado' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('disciplinas')->insert([
            'nombre' => 'Ecología de poblaciones',
            'estado' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('disciplinas')->insert([
            'nombre' => 'Ecología y conservación',
            'estado' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('disciplinas')->insert([
            'nombre' => 'Economía regional; sectorial e institucional',
            'estado' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('disciplinas')->insert([
            'nombre' => 'Enfermedades crónicas y degenerativas',
            'estado' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('disciplinas')->insert([
            'nombre' => 'Escenarios climáticos futuros y sus potenciales impactos',
            'estado' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('disciplinas')->insert([
            'nombre' => 'Estadísticas y probabilidades',
            'estado' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('disciplinas')->insert([
            'nombre' => 'Estado; actores; conflictos sociales; ciudadanía y democracia',
            'estado' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('disciplinas')->insert([
            'nombre' => 'Ética profesional; trabajo social y derechos humanos',
            'estado' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('disciplinas')->insert([
            'nombre' => 'Física de la materia condensada',
            'estado' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('disciplinas')->insert([
            'nombre' => 'Gerencia; administración funcional y gobierno',
            'estado' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('disciplinas')->insert([
            'nombre' => 'Gestión; políticas y didáctica educativas',
            'estado' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('disciplinas')->insert([
            'nombre' => 'Gestión pública; de empresa; de proyectos y recursos humanos',
            'estado' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('disciplinas')->insert([
            'nombre' => 'Historia regional y comtemporánea',
            'estado' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('disciplinas')->insert([
            'nombre' => 'Humanidades; investigación y sociedad',
            'estado' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('disciplinas')->insert([
            'nombre' => 'Manejo sostenible de recursos biológicos',
            'estado' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('disciplinas')->insert([
            'nombre' => 'Matemáticas',
            'estado' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('disciplinas')->insert([
            'nombre' => 'Materiales compuestos heterogéneos',
            'estado' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('disciplinas')->insert([
            'nombre' => 'Metabolismo; fisiología y fisiopatología',
            'estado' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('disciplinas')->insert([
            'nombre' => 'Microbiología; parasitología e inmunología',
            'estado' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('disciplinas')->insert([
            'nombre' => 'Microestructura y propiedades de materiales metálicos',
            'estado' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('disciplinas')->insert([
            'nombre' => 'Nano materiales avanzados para diversas aplicaciones',
            'estado' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('disciplinas')->insert([
            'nombre' => 'Neurociencias',
            'estado' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('disciplinas')->insert([
            'nombre' => 'Nutrición humana y seguridad alimentaria',
            'estado' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('disciplinas')->insert([
            'nombre' => 'Políticas públicas; de protección social y de grupos en situación de vulerabilidad',
            'estado' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('disciplinas')->insert([
            'nombre' => 'Productos naturales',
            'estado' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('disciplinas')->insert([
            'nombre' => 'Química ambiental',
            'estado' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('disciplinas')->insert([
            'nombre' => 'Reciclaje y valorización de residuos',
            'estado' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('disciplinas')->insert([
            'nombre' => 'Recursos hídricos; energéticos; geológicos y edaficos',
            'estado' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('disciplinas')->insert([
            'nombre' => 'Redes TIC',
            'estado' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('disciplinas')->insert([
            'nombre' => 'Remediación y recuperación de ambientes degradados',
            'estado' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('disciplinas')->insert([
            'nombre' => 'Salud mental',
            'estado' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('disciplinas')->insert([
            'nombre' => 'Salud pública y entornos saludables',
            'estado' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('disciplinas')->insert([
            'nombre' => 'Sistemas inteligentes',
            'estado' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('disciplinas')->insert([
            'nombre' => 'Taxonomía',
            'estado' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('disciplinas')->insert([
            'nombre' => 'Zoología',
            'estado' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
    }
}
