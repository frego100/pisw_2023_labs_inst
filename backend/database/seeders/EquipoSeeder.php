<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class EquipoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('equipos')->insert([
            'registro_id' => 1,
            'nombre' => 'equipo 01',
            'marca_modelo' => 'marca de equipo 01',
            'fecha_adquisicion' => '2022-01-01 12:00:00',
            'proveedor' => 'proveedor de prueba',
            'contacto_proveedor' => 'contacto de prueba',
            'codigo_patrimonio' => 'XXXXXXX-XXX',
            'accesorio' => 'Accesorio de prueba',
            'insumos' => 'Insumos de prueba',
            'descripcion' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('equipos')->insert([
            'registro_id' => 2,
            'nombre' => 'equipo 02',
            'marca_modelo' => 'marca de equipo 02',
            'fecha_adquisicion' => '2022-01-01 12:00:00',
            'proveedor' => 'proveedor de prueba',
            'contacto_proveedor' => 'contacto de prueba',
            'codigo_patrimonio' => 'XXXXXXX-XXX',
            'accesorio' => 'Accesorio de prueba',
            'insumos' => 'Insumos de prueba',
            'descripcion' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('equipos')->insert([
            'registro_id' => 3,
            'nombre' => 'equipo 03',
            'marca_modelo' => 'marca de equipo 03',
            'fecha_adquisicion' => '2022-01-01 12:00:00',
            'proveedor' => 'proveedor de prueba',
            'contacto_proveedor' => 'contacto de prueba',
            'codigo_patrimonio' => 'XXXXXXX-XXX',
            'accesorio' => 'Accesorio de prueba',
            'insumos' => 'Insumos de prueba',
            'descripcion' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
    }
}
