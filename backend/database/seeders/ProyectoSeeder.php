<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class ProyectoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('proyectos')->insert([
            'registro_id' => 1,
            'nombre_proyecto' => 'Proyecto de prueba 01',
            'investigador_principal' => 'Investigador 01',
            'coinvestigadores' => json_encode(['coinvestigador1' => 'AAAAAAAAA', 'coinvestigador3' => 'BBBBBBB']),
            'doi' => 'doi.org/10.1038/',
            'resumen' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
            'iba' => 'xxx-xx-xxxx',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        DB::table('proyectos')->insert([
            'registro_id' => 2,
            'nombre_proyecto' => 'Proyecto de prueba 02',
            'investigador_principal' => 'Investigador 02',
            'coinvestigadores' => json_encode(['coinvestigador1' => 'AAAAAAAAA']),
            'doi' => 'doi.org/10.1038/',
            'resumen' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
            'iba' => 'xxx-xx-xxxx',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        DB::table('proyectos')->insert([
            'registro_id' => 3,
            'nombre_proyecto' => 'Proyecto de prueba 03',
            'investigador_principal' => 'Investigador 03',
            'coinvestigadores' => json_encode(['coinvestigador1' => 'AAAAAAAAA', 'coinvestigador3' => 'BBBBBBB']),
            'doi' => 'doi.org/10.1038/',
            'resumen' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
            'iba' => 'xxx-xx-xxxx',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
    }
}
