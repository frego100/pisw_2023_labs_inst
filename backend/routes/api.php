<?php

use App\Http\Controllers\AreaController;
use App\Http\Controllers\DisciplinaController;
use App\Http\Controllers\EnvioCorreoController;
use App\Http\Controllers\LaboratorioController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\ProyectoController;
use App\Http\Controllers\RolController;
use App\Http\Controllers\ServicioInstitutoController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\DocumentoController;
use App\Http\Controllers\RegistroLaboratorioController;
use App\Http\Controllers\EquipoController;
use App\Http\Controllers\PoiController;
use App\Http\Controllers\PublicacionController;
use App\Http\Controllers\AsignarOperadorController;
use App\Http\Controllers\AsignarOperadorInstitutoController;
use App\Http\Controllers\InstitutoController;
use App\Http\Controllers\SolicitudController;
use App\Http\Controllers\GaleriaLaboratorioController;
use App\Http\Controllers\ProyectoInstitutoController;
use App\Http\Controllers\FuncionesInstitutoController;
use App\Http\Controllers\PublicacionInstitutoController;
use App\Http\Controllers\ConvenioIntitutoController;
use App\Http\Controllers\GoogleAuthController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/auth/google', [GoogleAuthController::class, 'verifyGoogleToken']);

// ENVÍO DE CORREO ELECTRÓNICO
Route::post('/correo', [EnvioCorreoController::class, 'correo']);

Route::post('/login', [LoginController::class, 'login']);

Route::middleware(['auth:sanctum'])->group(function () {
  // Route::get('/users', [LoginController::class, 'lista']);
  Route::post('/logout', [LoginController::class, 'logout']);

  //Mostrar usuarios
  Route::get('/usuarios', [UserController::class, 'index']);
  //Listar Coordinadores
  Route::get('/usuarios/coordinadores', [UserController::class, 'getCoordinadores']);
  //Listar Operadores
  Route::get('/usuarios/operadores', [UserController::class, 'getOperadores']);

  //Busqueda por nombre de usuario
  Route::get('/usuarios/getAllByNombreUsuario/', [UserController::class, 'getAllByNombreUsuarios']);
  //Busqueda por nombre de email
  Route::get('/usuarios/getAllByEmail/', [UserController::class, 'getAllByEmails']);
  //Busqueda por nombre de usuario
  Route::get('/usuarios/getAllByNombreUsuario/{nombreUsurio}', [UserController::class, 'getAllByNombreUsuario']);
  //Busqueda por nombre de email
  Route::get('/usuarios/getAllByEmail/{email}', [UserController::class, 'getAllByEmail']);

  //mostrar usuario  por id-probado
  Route::get('/usuarios/{id}', [UserController::class, 'show']);
  //Registrar usuario
  Route::post('/usuarios', [UserController::class, 'store']);
  //Actualizar usuario  por id
  Route::put('/usuarios/{idusuario}', [UserController::class, 'update']);
  //Eliminar usuario por id
  Route::delete('/usuarios/{idusuario}', [UserController::class, 'delete']);

  //Mostrar roles
  Route::get('/roless', [RolController::class, 'index']);
  //Registrar rol
  Route::post('/roless', [RolController::class, 'store']);
  //Mostrar rol por id
  Route::get('/roless/{id}', [RolController::class, 'show']);
  //Actualizar rol  por id
  Route::put('/roless/{idrol}', [RolController::class, 'update']);

  //Listar equipos
  Route::get('/coordinador/equipos/{idregistrarlaboratio}', [EquipoController::class, 'index']);
  //Registrar proyecto
  Route::post('/coordinador/equipos/', [EquipoController::class, 'store']);
  //Mostrar proyecto por id
  //Route::get('/operador/equipos/{idequipo}', [EquipoController::class, 'show']);
  //Actualizar proyecto
  Route::post('/coordinador/equipos/{idequipo}', [EquipoController::class, 'update']);
  //Eliminar proyecto por id
  Route::delete('/coordinador/equipos/{idequipo}', [EquipoController::class, 'delete']);

  //RUTAS SOLICITUDES DE MANTENIMIENTO
  //Crea una nueva solicitud
  Route::post('/coordinador/solicitud', [SolicitudController::class, 'store']);

  //Mostrar mostrar proyectos
  Route::get('/coordinador/proyectos/{idregistrarlaboratio}', [ProyectoController::class, 'index']);
  //Registrar proyecto
  Route::post('/coordinador/proyectos/', [ProyectoController::class, 'store']);
  //Mostrar proyecto por id
  Route::get('/coordinador/proyectos/show/{idproyecto}', [ProyectoController::class, 'show']);
  //Actualizar proyecto
  Route::post('/coordinador/proyectos/update/{idproyecto}', [ProyectoController::class, 'update']);
  //Eliminar proyecto por id
  Route::delete('/coordinador/proyectos/{idproyecto}', [ProyectoController::class, 'delete']);
  //Cambiar etapa de un proyecto
  Route::put('/coordinador/proyecto/cambiarEtapa/{idproyecto}', [ProyectoController::class, 'cambiarEtapa']);

  // Listar publicaciones
  Route::get('/coordinador/publicaciones/{idregistrarlaboratorio}', [PublicacionController::class, 'index']);
  // Mostrar una publicacion
  Route::get('/coordinador/publicacion/show/{idpublicacion}', [PublicacionController::class, 'show']);
  // Agregar una nueva publicacion
  Route::post('/coordinador/publicacion', [PublicacionController::class, 'store']);
  // Editar una publicacion
  Route::put('/coordinador/publicacion/{idpublicacion}', [PublicacionController::class, 'update']);
  // Eliminar una publicacion
  Route::delete('/coordinador/publicacion/{idpublicacion}', [PublicacionController::class, 'delete']);

  // Listar servicios
  Route::get('/coordinador/servicios/{idregistrarlaboratorio}', [RegistroLaboratorioController::class, 'listarServicios']);
  // Agregar un servicio
  Route::post('/coordinador/servicio/{idregistrarlaboratorio}', [RegistroLaboratorioController::class, 'ingresarServicio']);
  // Modificar servicio
  Route::put('/coordinador/servicio/editar/{idregistrarlaboratorio}', [RegistroLaboratorioController::class, 'modificarServicio']);
  // Eliminar servicio
  Route::put('/coordinador/servicio/eliminar/{idregistrarlaboratorio}', [RegistroLaboratorioController::class, 'eliminarServicio']);

  Route::get('/coordinador/galeriaLaboratorios/{idlaboratorio}', [GaleriaLaboratorioController::class, 'index']);
  // Registrar un una nueva imagen de un laboratorio
  Route::post('/coordinador/galeriaLaboratorio', [GaleriaLaboratorioController::class, 'store']);
  // Mostrar un registro de galeria laboratorio
  Route::get('/coordinador/galeriaLaboratorio/{idgaleria}', [GaleriaLaboratorioController::class, 'show']);
  // Editar un registro de galeria laboratorio
  Route::post('/coordinador/galeriaLaboratorio/{idgaleria}', [GaleriaLaboratorioController::class, 'update']);
  // Elimina un registro de galeria laboratorio
  Route::delete('/coordinador/galeriaLaboratorio/{idgaleria}', [GaleriaLaboratorioController::class, 'destroy']);

  //Lista servicios por id de instituto
  Route::get('/directores/servicios/{id_instituto}', [ServicioInstitutoController::class, 'index']);
  //Registra servicios
  Route::post('/directores/servicios', [ServicioInstitutoController::class, 'store']);
  //Busca un solo servicio
  Route::get('/directores/servicios/show/{idfuncion}', [ServicioInstitutoController::class, 'show']);
  //Actualiza servicio
  Route::put('/directores/servicios/{idfuncion}', [ServicioInstitutoController::class, 'update']);
  //Elimina servicio
  Route::delete('/directores/servicios/eliminar/{idfuncion}', [ServicioInstitutoController::class, 'delete']);

  Route::get('/directores/funciones/{id_instituto}', [FuncionesInstitutoController::class, 'index']);
  //Registra funciones
  Route::post('/directores/funciones', [FuncionesInstitutoController::class, 'store']);
  //Busca una sola funcion
  Route::get('/directores/funciones/show/{idfuncion}', [FuncionesInstitutoController::class, 'show']);
  //Actualiza funcion
  Route::post('/directores/funciones/{idfuncion}', [FuncionesInstitutoController::class, 'update']);
  //Elimina funcion
  Route::delete('/directores/funciones/eliminar/{idfuncion}', [FuncionesInstitutoController::class, 'delete']);

  // Lista todos los proyectos de un instituto
  Route::get('/proyectos/instituto/{idinstituto}', [ProyectoInstitutoController::class, 'index']);
  // Crea un proyecto de un instituto
  Route::post('/proyecto/instituto', [ProyectoInstitutoController::class, 'store']);
  // Muestra un proyecto de un instituto
  Route::get('/proyecto/instituto/{idproyecto}', [ProyectoInstitutoController::class, 'show']);
  // Actualiza un proyecto de un instituto
  Route::post('/proyecto/instituto/update/{idproyecto}', [ProyectoInstitutoController::class, 'update']);
  // Elimina un proyecto de un instituto
  Route::delete('/proyecto/instituto/{idproyecto}', [ProyectoInstitutoController::class, 'destroy']);

  // //RUTAS POIs
  Route::get('/director/pois/{id_instituto}', [PoiController::class, 'index']);
  // //Registrar poi
  Route::post('/director/pois/', [PoiController::class, 'store']);
  // //Mostrar poi por id
  Route::get('/director/pois/show/{idpoi}', [PoiController::class, 'show']);
  // //Actualizar poi
  Route::post('/director/pois/update/{idpoi}', [PoiController::class, 'update']);
  // //Eliminar poi por id
  Route::delete('/director/pois/{idpoi}', [PoiController::class, 'delete']);
  // descargar
  Route::get('/director/pois/download/{idpoi}', [PoiController::class, 'download']);

  // RUTA PARA PUBLICACIONES DE UN INSTITUTO
  // Listar las publicaciones de un instituto
  Route::get('/publicacion/instituto/{idinstituto}', [PublicacionInstitutoController::class, 'index']);
  // Craer una nueva publicacion de instituto
  Route::post('/publicacion/instituto', [PublicacionInstitutoController::class, 'store']);
  // Mostrar una publicacion de instituto
  Route::get('/publicacion/instituto/show/{idpublicacion}', [PublicacionInstitutoController::class, 'show']);
  // Editar una publicacion de instituto
  Route::put('/publicacion/instituto/{idpublicacion}', [PublicacionInstitutoController::class, 'update']);
  // Eliminar una publicacion de instituto
  Route::delete('/publicacion/instituto/{idpublicacion}', [PublicacionInstitutoController::class, 'destroy']);

  // RUTA PARA CONVENIOS DE UN INSTITUTO
  //Mostrar convenios por id instituto
  Route::get('/directores/convenios/{idinstituto}', [ConvenioIntitutoController::class, 'index']);
  //Registra convenios
  Route::post('/directores/convenios', [ConvenioIntitutoController::class, 'store']);
  //Busca una sola convenios
  Route::get('/directores/convenios/show/{idfuncion}', [ConvenioIntitutoController::class, 'show']);
  //Actualiza convenios
  Route::put('/directores/convenios/{idfuncion}', [ConvenioIntitutoController::class, 'update']);
  //Elimina convenios
  Route::delete('/directores/convenios/eliminar/{idfuncion}', [ConvenioIntitutoController::class, 'delete']);


  //APIS APLICADAS SOLO PARA EL ADMINISTRADOR
  Route::middleware(['auth:sanctum', 'admin'])->group(function () {  //Doble middleware para verificar que este logueado y que sea admin

    Route::get('/registroLaboratorios', [RegistroLaboratorioController::class, 'VisLab']);

    //Muestra todos los intitutos
    Route::get('/institutoss', [InstitutoController::class, 'index']);

    Route::put('/registroLaboratorios/completar/{idregistrarlaboratio}', [RegistroLaboratorioController::class, 'completar']);
    
    /*** ROLES ***/
    //Mostrar roles
    Route::get('/roles', [RolController::class, 'index']);
    //Registrar rol
    Route::post('/roles', [RolController::class, 'store']);
    //Mostrar rol por id
    Route::get('/roles/{id}', [RolController::class, 'show']);
    //Actualizar rol  por id
    Route::put('/roles/{idrol}', [RolController::class, 'update']);
    //Eliminar rol por id
    Route::delete('/roles/{idrol}', [RolController::class, 'delete']);


    // asignacion de coordinaores
    Route::post('/registroLaboratorio', [RegistroLaboratorioController::class, 'store']);
    Route::delete('/registroLaboratorio/{id}', [RegistroLaboratorioController::class, 'delete']);

    //Actualiza un laboratorio
    Route::put('/registroLaboratorio/{id}', [RegistroLaboratorioController::class, 'update']);
    /*** LABORATORIOS ***/
    //Mostrar laboratorios
    Route::get('/laboratorios', [LaboratorioController::class, 'index']);
    //Registrar laboratorios
    Route::post('/laboratorios', [LaboratorioController::class, 'store']);
    //Mostrar laboratorio por id
    Route::get('/laboratorios/{idlaboratorio}', [LaboratorioController::class, 'show']);
    //Actualizar laboratorio por id
    Route::put('/laboratorios/{idlaboratorio}', [LaboratorioController::class, 'update']);
    //Eliminar laboratorio por id
    Route::delete('/laboratorios/{idlaboratorio}', [LaboratorioController::class, 'delete']);

    /*** AREAS ***/
    //Mostrar areas
    Route::get('/areas', [AreaController::class, 'index']);
    //Registrar areas
    Route::post('/areas', [AreaController::class, 'store']);
    //Mostrar area por id
    Route::get('/areas/{idarea}', [AreaController::class, 'show']);
    //Actualizar areas  por id
    Route::put('/areas/{idarea}', [AreaController::class, 'update']);
    //Eliminar areas por id
    Route::delete('/areas/{idarea}', [AreaController::class, 'delete']);

    /*** DISCIPLINAS ***/
    //Mostrar disciplinas
    Route::get('/disciplinas', [DisciplinaController::class, 'index']);
    //Registrar disciplinas
    Route::post('/disciplinas', [DisciplinaController::class, 'store']);
    //Mostrar disciplina por id
    Route::get('/disciplinas/{iddisciplina}', [DisciplinaController::class, 'show']);
    //Actualizar disciplina por id
    Route::put('/disciplinas/{iddisciplina}', [DisciplinaController::class, 'update']);
    //Eliminar disciplina por id
    Route::delete('/disciplinas/{iddisciplina}', [DisciplinaController::class, 'delete']);

    //Listar los operadores
    Route::get('/admin/operadores/listar', [UserController::class, 'listarOperadores']);
    //Crear operador
    Route::post('/admin/operadores/crear', [UserController::class, 'crearOperadorDesdeCoordinador']);

    //SOLICITUDES
    //Lista todas las solicitudes
    Route::get('/solicitudes', [SolicitudController::class, 'index']);
    //Obtener una solicitud
    Route::get('/solicitud/{idsolicitud}', [SolicitudController::class, 'show']);
    //Actualiza el estado de una solicitud
    Route::put('/solicitud/{idsolicitud}', [SolicitudController::class, 'update']);
    Route::delete('/solicitud/{idsolicitud}', [SolicitudController::class, 'delete']);
    
  });

  //APIS APLICADAS PARA COORDINADORES
  Route::middleware(['auth:sanctum', 'coordinador'])->group(function () {
    //Muestra los laboratorios de un coordinador
    Route::get('/registroLaboratorio/{id}', [RegistroLaboratorioController::class, 'VisLabCoordinador']);
    
    //Muestra la opcion de los Laboratorios, Areas y Disciplinas
    Route::get('/info/laboratoriosareasdisciplinas', [RegistroLaboratorioController::class, 'VistaRegLab']);
    //Asigna un coordinador a un laboratorio
    //Route::post('/registroLaboratorio', [RegistroLaboratorioController::class, 'store']);
    //Asigna un coordinador a un laboratorio
    //Completar info de un laboratorio
    Route::put('/registroLaboratorio/completar/{idregistrarlaboratio}', [RegistroLaboratorioController::class, 'completar']);


    //Mostrar documentos
    Route::get('/coordinador/documentos', [DocumentoController::class, 'index']);
    //Agregar documentos
    Route::post('/coordinador/documentos', [DocumentoController::class, 'store']);
    //Mostrar un documento
    Route::get('/coordinador/documentos/{iddocumento}', [DocumentoController::class, 'show']);
    //Editar documentos
    Route::post('/coordinador/documentos/update/{iddocumento}', [DocumentoController::class, 'update']);
    //Eliminar documentos
    Route::delete('/coordinador/documentos/{iddocumento}', [DocumentoController::class, 'delete']);
    //Descargar documentos
    Route::get('/coordinador/documentos/download/{id}', [DocumentoController::class, 'download']);

    //Mostrar mostrar proyectos
    /*Route::get('/coordinador/proyectos/{idregistrarlaboratio}', [ProyectoController::class, 'index']);
    //Registrar proyecto
    Route::post('/coordinador/proyectos/', [ProyectoController::class, 'store']);
    //Mostrar proyecto por id
    Route::get('/coordinador/proyectos/show/{idproyecto}', [ProyectoController::class, 'show']);
    //Actualizar proyecto
    Route::post('/coordinador/proyectos/update/{idproyecto}', [ProyectoController::class, 'update']);
    //Eliminar proyecto por id
    Route::delete('/coordinador/proyectos/{idproyecto}', [ProyectoController::class, 'delete']);
    //Cambiar etapa de un proyecto
    Route::put('/coordinador/proyecto/cambiarEtapa/{idproyecto}', [ProyectoController::class, 'cambiarEtapa']);*/

    //Listar los operadores
    Route::get('/coordinador/operadores/listar', [UserController::class, 'listarOperadores']);
    //Crear operador
    Route::post('/coordinador/operadores/crear', [UserController::class, 'crearOperadorDesdeCoordinador']);

    Route::delete('/coordinador/{idusuario}', [UserController::class, 'deleteOperadorFromCoordinador']);

    //Listar los operadores asignados a un laboratorio
    Route::get('/coordinador/operadoresLaboratorio/{idregistrarlaboratorio}', [AsignarOperadorController::class, 'listarOperadoresLaboratorio']);
    //Listar los operadores asignados a un laboratorio
    Route::post('/coordinador/asignarOperadores/{idregistrarlaboratorio}', [AsignarOperadorController::class, 'asignarOperadorLaboratorio']);

    //EQUIPOS
    //RUTAS EQUIPOS
    /*Route::get('/coordinador/equipos/{idregistrarlaboratio}', [EquipoController::class, 'index']);
    //Registrar proyecto
    Route::post('/coordinador/equipos/', [EquipoController::class, 'store']);
    //Mostrar proyecto por id
    //Route::get('/operador/equipos/{idequipo}', [EquipoController::class, 'show']);
    //Actualizar proyecto
    Route::post('/coordinador/equipos/{idequipo}', [EquipoController::class, 'update']);
    //Eliminar proyecto por id
    Route::delete('/coordinador/equipos/{idequipo}', [EquipoController::class, 'delete']);
    Route::get('/coordinador/equipos/{idequipo}', [EquipoController::class, 'show']);*/

    //RUTAS SOLICITUDES DE MANTENIMIENTO
    //Crea una nueva solicitud
    //Route::post('/coordinador/solicitud', [SolicitudController::class, 'store']);

    //RUTAS PARA SERVICIOS
    // Listar servicios
    /*Route::get('/coordinador/servicios/{idregistrarlaboratorio}', [RegistroLaboratorioController::class, 'listarServicios']);
    // Agregar un servicio
    Route::post('/coordinador/servicio/{idregistrarlaboratorio}', [RegistroLaboratorioController::class, 'ingresarServicio']);
    // Modificar servicio
    Route::put('/coordinador/servicio/editar/{idregistrarlaboratorio}', [RegistroLaboratorioController::class, 'modificarServicio']);
    // Eliminar servicio
    Route::put('/coordinador/servicio/eliminar/{idregistrarlaboratorio}', [RegistroLaboratorioController::class, 'eliminarServicio']);*/

    //RUTAS PARA PUBLICACIONES
    // Listar publicaciones
    /*Route::get('/coordinador/publicaciones/{idregistrarlaboratorio}', [PublicacionController::class, 'index']);
    // Mostrar una publicacion
    Route::get('/coordinador/publicacion/show/{idpublicacion}', [PublicacionController::class, 'show']);
    // Agregar una nueva publicacion
    Route::post('/coordinador/publicacion', [PublicacionController::class, 'store']);
    // Editar una publicacion
    Route::put('/coordinador/publicacion/{idpublicacion}', [PublicacionController::class, 'update']);
    // Eliminar una publicacion
    Route::delete('/coordinador/publicacion/{idpublicacion}', [PublicacionController::class, 'delete']);*/

    // RUTAS PARA GALERIA DE FOTOS DE LABORATORIO
    // Listar la galeria de fotos de un laboratorio
    /*Route::get('/coordinador/galeriaLaboratorios/{idlaboratorio}', [GaleriaLaboratorioController::class, 'index']);
    // Registrar un una nueva imagen de un laboratorio
    Route::post('/coordinador/galeriaLaboratorio', [GaleriaLaboratorioController::class, 'store']);
    // Mostrar un registro de galeria laboratorio
    Route::get('/coordinador/galeriaLaboratorio/{idgaleria}', [GaleriaLaboratorioController::class, 'show']);
    // Editar un registro de galeria laboratorio
    Route::post('/coordinador/galeriaLaboratorio/{idgaleria}', [GaleriaLaboratorioController::class, 'update']);
    // Elimina un registro de galeria laboratorio
    Route::delete('/coordinador/galeriaLaboratorio/{idgaleria}', [GaleriaLaboratorioController::class, 'destroy']);*/
  });


  //APIS APLICADAS A PARA OPERADORES
  Route::middleware(['auth:sanctum', 'operador'])->group(function () {
    //Mostrar mostrar proyectos
    Route::get('/operador/proyectos/{idregistrarlaboratio}', [ProyectoController::class, 'index']);
    //Registrar proyecto
    Route::post('/operador/proyectos/', [ProyectoController::class, 'store']);
    //Mostrar proyecto por id
    Route::get('/operador/proyectos/show/{idproyecto}', [ProyectoController::class, 'show']);
    //Actualizar proyecto
    Route::post('/operador/proyectos/update/{idproyecto}', [ProyectoController::class, 'update']);
    //Eliminar proyecto por id
    Route::delete('/operador/proyectos/{idproyecto}', [ProyectoController::class, 'delete']);
    // Route::put('/operador/proyectos/{idproyecto}', [ProyectoController::class, 'delete']);

    //Cambiar etapa de un proyecto
    Route::put('/operador/proyecto/cambiarEtapa/{idproyecto}', [ProyectoController::class, 'cambiarEtapa']);

    Route::get('/vermislabs/{id}', [UserController::class, 'verMisLaboratoriosOperador']);
    //Actualizar proyecto

    Route::get('/vermisinsts/{id}', [UserController::class, 'verMisInstitutosOperador']);
    //Actualizar proyecto

    /*** DOCUMENTOS ***/
    //Mostrar documentos
    Route::get('/documentos', [DocumentoController::class, 'index']);
    //Agregar documentos
    Route::post('/documentos', [DocumentoController::class, 'store']);
    //Mostrar un documento
    Route::get('/documentos/{iddocumento}', [DocumentoController::class, 'show']);
    //Editar documentos
    Route::post('/documentos/update/{iddocumento}', [DocumentoController::class, 'update']);
    //Eliminar documentos
    Route::delete('/documentos/{iddocumento}', [DocumentoController::class, 'delete']);
    //Descargar documentos
    Route::get('documentos/download/{id}', [DocumentoController::class, 'download']);

    //RUTAS EQUIPOS
    Route::get('/operador/equipos/{idregistrarlaboratio}', [EquipoController::class, 'index']);
    //Registrar equipo
    Route::post('/operador/equipos/', [EquipoController::class, 'store']);
    //Mostrar equipo por id
    Route::get('/operador/equipos/show/{idequipo}', [EquipoController::class, 'show']);
    //Actualizar equipo
    Route::post('/operador/equipos/{idequipo}', [EquipoController::class, 'update']);
    //Eliminar equipo por id
    Route::delete('/operador/equipos/{idequipo}', [EquipoController::class, 'delete']);

    //RUTAS SOLICITUDES DE MANTENIMIENTO
    //Crea una nueva solicitud
    Route::post('/operador/solicitud', [SolicitudController::class, 'store']);

    //RUTAS PARA SERVICIOS
    // Listar servicios
    Route::get('/operador/servicios/{idregistrarlaboratorio}', [RegistroLaboratorioController::class, 'listarServicios']);
    // Agregar un servicio
    Route::post('/operador/servicio/{idregistrarlaboratorio}', [RegistroLaboratorioController::class, 'ingresarServicio']);
    // Editar un servicio
    Route::put('/operador/servicio/editar/{idregistrarlaboratorio}', [RegistroLaboratorioController::class, 'modificarServicio']);
    // Eliminar un servicio
    Route::put('/operador/servicio/eliminar/{idregistrarlaboratorio}', [RegistroLaboratorioController::class, 'eliminarServicio']);

    //RUTAS PARA PUBLICACIONES
    // Listar publicaciones
    Route::get('/operador/publicaciones/{idregistrarlaboratorio}', [PublicacionController::class, 'index']);
    // Mostrar una publicacion
    Route::get('/operador/publicacion/show/{idpublicacion}', [PublicacionController::class, 'show']);
    // Agregar una nueva publicacion
    Route::post('/operador/publicacion', [PublicacionController::class, 'store']);
    // Editar una publicacion
    Route::put('/operador/publicacion/{idpublicacion}', [PublicacionController::class, 'update']);
    // Eliminar una publicacion
    Route::delete('/operador/publicacion/{idpublicacion}', [PublicacionController::class, 'delete']);

    // RUTAS PARA GALERIA DE FOTOS DE LABORATORIO
    // Listar la galeria de fotos de un laboratorio
    Route::get('/operador/galeriaLaboratorios/{idlaboratorio}', [GaleriaLaboratorioController::class, 'index']);
    // Registrar un una nueva imagen de un laboratorio
    Route::post('/operador/galeriaLaboratorio', [GaleriaLaboratorioController::class, 'store']);
    // Mostrar un registro de galeria laboratorio
    Route::get('/operador/galeriaLaboratorio/{idgaleria}', [GaleriaLaboratorioController::class, 'show']);
    // Editar un registro de galeria laboratorio
    Route::post('/operador/galeriaLaboratorio/{idgaleria}', [GaleriaLaboratorioController::class, 'update']);
    // Elimina un registro de galeria laboratorio
    Route::delete('/operador/galeriaLaboratorio/{idgaleria}', [GaleriaLaboratorioController::class, 'destroy']);
  });

  Route::middleware(['auth:sanctum', 'adminOrDirector'])->group(function () {

    //RUTAS INSTITUTOS
    Route::get('/institutos/director/{usuario_director}', [InstitutoController::class, 'indexByDirector']);
    //Registrar instituto
    Route::post('/institutos', [InstitutoController::class, 'store']);
    //Mostrar instituto por id
    Route::get('/institutos/show/{idinstituto}', [InstitutoController::class, 'show']);
    //Actualizar instituto
    Route::put('/institutos/{idinstituto}', [InstitutoController::class, 'update']);

    Route::put('/institutos/update/{idinstituto}', [InstitutoController::class, 'updateDirector']);
    //Eliminar instituto por id
    Route::delete('/institutos/{idinstituto}', [InstitutoController::class, 'delete']);

    //DIRECTORES
    //Lista todos los directores
    Route::get('/directores', [UserController::class, 'listarDirectores']);
    // //COmpletar la imfor de los
    Route::post('/directores/completar/{iddirector}', [UserController::class, 'completarInfoDirector']);
    // Lista de institutos asignados al director por Id
    Route::get('/directores/{usuario_director}', [InstitutoController::class, 'indexByDirector']);
    //Completar info de un instituto
    Route::post('/directores/completarInstituto/{idinstituto}', [InstitutoController::class, 'completarInstituto']);
    // Listar instituto de director
    Route::get('/institutosPorDirector/{usuario_director}', [InstitutoController::class, 'VistaInstitutoDirector']);
  });

  Route::middleware(['auth:sanctum', 'director'])->group(function () {

    Route::get('/comiteDirectivo', [UserController::class, 'indexComiteDirectivo']);
    Route::get('/comiteDirectivo/{idComteDirectivo}', [UserController::class, 'showComiteDirectivo']);
    Route::post('/comiteDirectivo', [UserController::class, 'storeComiteDirectivo']);
    Route::put('/comiteDirectivo/{idComteDirectivo}', [UserController::class, 'updateComiteDirectivo']);
    Route::delete('/comiteDirectivo/{idComteDirectivo}', [UserController::class, 'deleteComiteDiretivo']);

    // RUTA PARA LOS OPERADORES DE INSTITUTO
    //Listar los operadores asignados a un instituto
    Route::get('/operadoresInstituto/{idregistrarlaboratorio}', [AsignarOperadorInstitutoController::class, 'listarOperadoresInstituto']);
    //Listar los operadores asignados a un instituto
    Route::post('/asignarOperadoresInstituto/{idregistrarlaboratorio}', [AsignarOperadorInstitutoController::class, 'asignarOperadorInstituto']);

    // RUTA PARA LOS PROYECTOS DE INSTITUTO
    // Lista todos los proyectos de un instituto
    /*Route::get('/proyectos/instituto/{idinstituto}', [ProyectoInstitutoController::class, 'index']);
    // Crea un proyecto de un instituto
    Route::post('/proyecto/instituto', [ProyectoInstitutoController::class, 'store']);
    // Muestra un proyecto de un instituto
    Route::get('/proyecto/instituto/{idproyecto}', [ProyectoInstitutoController::class, 'show']);
    // Actualiza un proyecto de un instituto
    Route::post('/proyecto/instituto/update/{idproyecto}', [ProyectoInstitutoController::class, 'update']);
    // Elimina un proyecto de un instituto
    Route::delete('/proyecto/instituto/{idproyecto}', [ProyectoInstitutoController::class, 'destroy']);*/

    //RUTA PARA SERVICIOS DE UN INSTITUTO
    //Lista servicios por id de instituto
    /*Route::get('/directores/servicios/{id_instituto}', [ServicioInstitutoController::class, 'index']);
    //Registra servicios
    Route::post('/directores/servicios', [ServicioInstitutoController::class, 'store']);
    //Busca un solo servicio
    Route::get('/directores/servicios/show/{idfuncion}', [ServicioInstitutoController::class, 'show']);
    //Actualiza servicio
    Route::put('/directores/servicios/{idfuncion}', [ServicioInstitutoController::class, 'update']);
    //Elimina servicio
    Route::delete('/directores/servicios/eliminar/{idfuncion}', [ServicioInstitutoController::class, 'delete']);*/

    //RUTA PARA FUNCIONES DE UN INSTITUTO
    //Lista funciones por id de instituto
    /*Route::get('/directores/funciones/{id_instituto}', [FuncionesInstitutoController::class, 'index']);
    //Registra funciones
    Route::post('/directores/funciones', [FuncionesInstitutoController::class, 'store']);
    //Busca una sola funcion
    Route::get('/directores/funciones/show/{idfuncion}', [FuncionesInstitutoController::class, 'show']);
    //Actualiza funcion
    Route::post('/directores/funciones/{idfuncion}', [FuncionesInstitutoController::class, 'update']);
    //Elimina funcion
    Route::delete('/directores/funciones/eliminar/{idfuncion}', [FuncionesInstitutoController::class, 'delete']);*/

    // RUTA PARA PUBLICACIONES DE UN INSTITUTO
    // Listar las publicaciones de un instituto
    /*Route::get('/publicacion/instituto/{idinstituto}', [PublicacionInstitutoController::class, 'index']);
    // Craer una nueva publicacion de instituto
    Route::post('/publicacion/instituto', [PublicacionInstitutoController::class, 'store']);
    // Mostrar una publicacion de instituto
    Route::get('/publicacion/instituto/show/{idpublicacion}', [PublicacionInstitutoController::class, 'show']);
    // Editar una publicacion de instituto
    Route::put('/publicacion/instituto/{idpublicacion}', [PublicacionInstitutoController::class, 'update']);
    // Eliminar una publicacion de instituto
    Route::delete('/publicacion/instituto/{idpublicacion}', [PublicacionInstitutoController::class, 'destroy']);*/

    // //POIs
    // //RUTAS POIs
    /*Route::get('/director/pois/{id_instituto}', [PoiController::class, 'index']);
    // //Registrar poi
    Route::post('/director/pois/', [PoiController::class, 'store']);
    // //Mostrar poi por id
    Route::get('/director/pois/show/{idpoi}', [PoiController::class, 'show']);
    // //Actualizar poi
    Route::post('/director/pois/update/{idpoi}', [PoiController::class, 'update']);
    // //Eliminar poi por id
    Route::delete('/director/pois/{idpoi}', [PoiController::class, 'delete']);
    // descargar
    Route::get('/director/pois/download/{idpoi}', [PoiController::class, 'download']);*/

    /**
     * Rutas de los POIS
     */
    //Route::apiResource('/instituto/pois', 'PoiController');


    // RUTA PARA CONVENIOS DE UN INSTITUTO
    //Mostrar convenios por id instituto
    /*Route::get('/directores/convenios/{idinstituto}', [ConvenioIntitutoController::class, 'index']);
    //Registra convenios
    Route::post('/directores/convenios', [ConvenioIntitutoController::class, 'store']);
    //Busca una sola convenios
    Route::get('/directores/convenios/show/{idfuncion}', [ConvenioIntitutoController::class, 'show']);
    //Actualiza convenios
    Route::put('/directores/convenios/{idfuncion}', [ConvenioIntitutoController::class, 'update']);
    //Elimina convenios
    Route::delete('/directores/convenios/eliminar/{idfuncion}', [ConvenioIntitutoController::class, 'delete']);*/
  });

  //   Route::middleware(['auth:sanctum', 'adminlaboratorios'])->group(function () {

  //     // asignacion de coordinaores
  //     Route::post('/registroLaboratorio', [RegistroLaboratorioController::class, 'store']);
  //     Route::delete('/registroLaboratorio/{id}', [RegistroLaboratorioController::class, 'delete']);

  //     //Actualiza un laboratorio
  //     Route::put('/registroLaboratorio/{id}', [RegistroLaboratorioController::class, 'update']);
  //     /*** LABORATORIOS ***/
  //     //Mostrar laboratorios
  //     Route::get('/laboratorios', [LaboratorioController::class, 'index']);
  //     //Registrar laboratorios
  //     Route::post('/laboratorios', [LaboratorioController::class, 'store']);
  //     //Mostrar laboratorio por id
  //     Route::get('/laboratorios/{idlaboratorio}', [LaboratorioController::class, 'show']);
  //     //Actualizar laboratorio por id
  //     Route::put('/laboratorios/{idlaboratorio}', [LaboratorioController::class, 'update']);
  //     //Eliminar laboratorio por id
  //     Route::delete('/laboratorios/{idlaboratorio}', [LaboratorioController::class, 'delete']);

  //     /*** AREAS ***/
  //     //Mostrar areas
  //     Route::get('/areas', [AreaController::class, 'index']);
  //     //Registrar areas
  //     Route::post('/areas', [AreaController::class, 'store']);
  //     //Mostrar area por id
  //     Route::get('/areas/{idarea}', [AreaController::class, 'show']);
  //     //Actualizar areas  por id
  //     Route::put('/areas/{idarea}', [AreaController::class, 'update']);
  //     //Eliminar areas por id
  //     Route::delete('/areas/{idarea}', [AreaController::class, 'delete']);

  //     /*** DISCIPLINAS ***/
  //     //Mostrar disciplinas
  //     Route::get('/disciplinas', [DisciplinaController::class, 'index']);
  //     //Registrar disciplinas
  //     Route::post('/disciplinas', [DisciplinaController::class, 'store']);
  //     //Mostrar disciplina por id
  //     Route::get('/disciplinas/{iddisciplina}', [DisciplinaController::class, 'show']);
  //     //Actualizar disciplina por id
  //     Route::put('/disciplinas/{iddisciplina}', [DisciplinaController::class, 'update']);
  //     //Eliminar disciplina por id
  //     Route::delete('/disciplinas/{iddisciplina}', [DisciplinaController::class, 'delete']);

  //     //Listar los operadores
  //     Route::get('/admin/operadores/listar', [UserController::class, 'listarOperadores']);
  //     //Crear operador
  //     Route::post('/admin/operadores/crear', [UserController::class, 'crearOperadorDesdeCoordinador']);

  //     //SOLICITUDES
  //     //Lista todas las solicitudes
  //     Route::get('/solicitudes', [SolicitudController::class, 'index']);
  //     //Obtener una solicitud
  //     Route::get('/solicitud/{idsolicitud}', [SolicitudController::class, 'show']);
  //     //Actualiza el estado de una solicitud
  //     Route::put('/solicitud/{idsolicitud}', [SolicitudController::class, 'update']);


  //     //RUTAS COORDINADOR----------------------------------------------------------------------------
  //     //Muestra los laboratorios de un coordinador
  //     Route::get('/registroLaboratorio/{id}', [RegistroLaboratorioController::class, 'VisLabCoordinador']);
  //     //Muestra la opcion de los Laboratorios, Areas y Disciplinas
  //     Route::get('/info/laboratoriosareasdisciplinas', [RegistroLaboratorioController::class, 'VistaRegLab']);
  //     //Asigna un coordinador a un laboratorio
  //     //Route::post('/registroLaboratorio', [RegistroLaboratorioController::class, 'store']);
  //     //Asigna un coordinador a un laboratorio
  //     //Completar info de un laboratorio
  //     Route::put('/registroLaboratorio/completar/{idregistrarlaboratio}', [RegistroLaboratorioController::class, 'completar']);


  //     //Mostrar documentos
  //     Route::get('/coordinador/documentos', [DocumentoController::class, 'index']);
  //     //Agregar documentos
  //     Route::post('/coordinador/documentos', [DocumentoController::class, 'store']);
  //     //Mostrar un documento
  //     Route::get('/coordinador/documentos/{iddocumento}', [DocumentoController::class, 'show']);
  //     //Editar documentos
  //     Route::post('/coordinador/documentos/update/{iddocumento}', [DocumentoController::class, 'update']);
  //     //Eliminar documentos
  //     Route::delete('/coordinador/documentos/{iddocumento}', [DocumentoController::class, 'delete']);
  //     //Descargar documentos
  //     Route::get('/coordinador/documentos/download/{id}', [DocumentoController::class, 'download']);

  //     //Mostrar mostrar proyectos
  //     Route::get('/coordinador/proyectos/{idregistrarlaboratio}', [ProyectoController::class, 'index']);
  //     //Registrar proyecto
  //     Route::post('/coordinador/proyectos/', [ProyectoController::class, 'store']);
  //     //Mostrar proyecto por id
  //     Route::get('/coordinador/proyectos/show/{idproyecto}', [ProyectoController::class, 'show']);
  //     //Actualizar proyecto
  //     Route::post('/coordinador/proyectos/update/{idproyecto}', [ProyectoController::class, 'update']);
  //     //Eliminar proyecto por id
  //     Route::delete('/coordinador/proyectos/{idproyecto}', [ProyectoController::class, 'delete']);
  //     //Cambiar etapa de un proyecto
  //     Route::put('/coordinador/proyecto/cambiarEtapa/{idproyecto}', [ProyectoController::class, 'cambiarEtapa']);

  //     //Listar los operadores
  //     Route::get('/coordinador/operadores/listar', [UserController::class, 'listarOperadores']);
  //     //Crear operador
  //     Route::post('/coordinador/operadores/crear', [UserController::class, 'crearOperadorDesdeCoordinador']);
  //     //Listar los operadores asignados a un laboratorio
  //     Route::get('/coordinador/operadoresLaboratorio/{idregistrarlaboratorio}', [AsignarOperadorController::class, 'listarOperadoresLaboratorio']);
  //     //Listar los operadores asignados a un laboratorio
  //     Route::post('/coordinador/asignarOperadores/{idregistrarlaboratorio}', [AsignarOperadorController::class, 'asignarOperadorLaboratorio']);

  //     //EQUIPOS
  //     //RUTAS EQUIPOS
  //     Route::get('/coordinador/equipos/{idregistrarlaboratio}', [EquipoController::class, 'index']);
  //     //Registrar proyecto
  //     Route::post('/coordinador/equipos/', [EquipoController::class, 'store']);
  //     //Mostrar proyecto por id
  //     //Route::get('/operador/equipos/{idequipo}', [EquipoController::class, 'show']);
  //     //Actualizar proyecto
  //     Route::put('/coordinador/equipos/{idequipo}', [EquipoController::class, 'update']);
  //     //Eliminar proyecto por id
  //     Route::put('/coordinador/equipos/{idequipo}', [EquipoController::class, 'delete']);
  //     Route::get('/coordinador/equipos/{idequipo}', [EquipoController::class, 'show']);

  //     //RUTAS SOLICITUDES DE MANTENIMIENTO
  //     //Crea una nueva solicitud
  //     Route::post('/coordinador/solicitud', [SolicitudController::class, 'store']);

  //     //RUTAS PARA SERVICIOS
  //     // Listar servicios
  //     Route::get('/coordinador/servicios/{idregistrarlaboratorio}', [RegistroLaboratorioController::class, 'listarServicios']);
  //     // Agregar un servicio
  //     Route::post('/coordinador/servicio/{idregistrarlaboratorio}', [RegistroLaboratorioController::class, 'ingresarServicio']);
  //     // Modificar servicio
  //     Route::put('/coordinador/servicio/editar/{idregistrarlaboratorio}', [RegistroLaboratorioController::class, 'modificarServicio']);
  //     // Eliminar servicio
  //     Route::put('/coordinador/servicio/eliminar/{idregistrarlaboratorio}', [RegistroLaboratorioController::class, 'eliminarServicio']);

  //     //RUTAS PARA PUBLICACIONES
  //     // Listar publicaciones
  //     Route::get('/coordinador/publicaciones/{idregistrarlaboratorio}', [PublicacionController::class, 'index']);
  //     // Mostrar una publicacion
  //     Route::get('/coordinador/publicacion/show/{idpublicacion}', [PublicacionController::class, 'show']);
  //     // Agregar una nueva publicacion
  //     Route::post('/coordinador/publicacion', [PublicacionController::class, 'store']);
  //     // Editar una publicacion
  //     Route::put('/coordinador/publicacion/{idpublicacion}', [PublicacionController::class, 'update']);
  //     // Eliminar una publicacion
  //     Route::delete('/coordinador/publicacion/{idpublicacion}', [PublicacionController::class, 'delete']);

  //     // RUTAS PARA GALERIA DE FOTOS DE LABORATORIO
  //     // Listar la galeria de fotos de un laboratorio
  //     Route::get('/coordinador/galeriaLaboratorios/{idlaboratorio}', [GaleriaLaboratorioController::class, 'index']);
  //     // Registrar un una nueva imagen de un laboratorio
  //     Route::post('/coordinador/galeriaLaboratorio', [GaleriaLaboratorioController::class, 'store']);
  //     // Mostrar un registro de galeria laboratorio
  //     Route::get('/coordinador/galeriaLaboratorio/{idgaleria}', [GaleriaLaboratorioController::class, 'show']);
  //     // Editar un registro de galeria laboratorio
  //     Route::post('/coordinador/galeriaLaboratorio/{idgaleria}', [GaleriaLaboratorioController::class, 'update']);
  //     // Elimina un registro de galeria laboratorio
  //     Route::delete('/coordinador/galeriaLaboratorio/{idgaleria}', [GaleriaLaboratorioController::class, 'destroy']);


  //     // RUTAS OPERADOR -------------------------------------------------------------------------------------
  //     //Mostrar mostrar proyectos
  //     Route::get('/operador/proyectos/{idregistrarlaboratio}', [ProyectoController::class, 'index']);
  //     //Registrar proyecto
  //     Route::post('/operador/proyectos/', [ProyectoController::class, 'store']);
  //     //Mostrar proyecto por id
  //     Route::get('/operador/proyectos/show/{idproyecto}', [ProyectoController::class, 'show']);
  //     //Actualizar proyecto
  //     Route::post('/operador/proyectos/update/{idproyecto}', [ProyectoController::class, 'update']);
  //     //Eliminar proyecto por id
  //     Route::delete('/operador/proyectos/{idproyecto}', [ProyectoController::class, 'delete']);
  //     // Route::put('/operador/proyectos/{idproyecto}', [ProyectoController::class, 'delete']);

  //     //Cambiar etapa de un proyecto
  //     Route::put('/operador/proyecto/cambiarEtapa/{idproyecto}', [ProyectoController::class, 'cambiarEtapa']);

  //     /*** DOCUMENTOS ***/
  //     //Mostrar documentos
  //     Route::get('/documentos', [DocumentoController::class, 'index']);
  //     //Agregar documentos
  //     Route::post('/documentos', [DocumentoController::class, 'store']);
  //     //Mostrar un documento
  //     Route::get('/documentos/{iddocumento}', [DocumentoController::class, 'show']);
  //     //Editar documentos
  //     Route::post('/documentos/update/{iddocumento}', [DocumentoController::class, 'update']);
  //     //Eliminar documentos
  //     Route::delete('/documentos/{iddocumento}', [DocumentoController::class, 'delete']);
  //     //Descargar documentos
  //     Route::get('documentos/download/{id}', [DocumentoController::class, 'download']);

  //     //RUTAS EQUIPOS
  //     Route::get('/operador/equipos/{idregistrarlaboratio}', [EquipoController::class, 'index']);
  //     //Registrar equipo
  //     Route::post('/operador/equipos/', [EquipoController::class, 'store']);
  //     //Mostrar equipo por id
  //     Route::get('/operador/equipos/show/{idequipo}', [EquipoController::class, 'show']);
  //     //Actualizar equipo
  //     Route::post('/operador/equipos/{idequipo}', [EquipoController::class, 'update']);
  //     //Eliminar equipo por id
  //     Route::delete('/operador/equipos/{idequipo}', [EquipoController::class, 'delete']);

  //     //RUTAS SOLICITUDES DE MANTENIMIENTO
  //     //Crea una nueva solicitud
  //     Route::post('/operador/solicitud', [SolicitudController::class, 'store']);

  //     //RUTAS PARA SERVICIOS
  //     // Listar servicios
  //     Route::get('/operador/servicios/{idregistrarlaboratorio}', [RegistroLaboratorioController::class, 'listarServicios']);
  //     // Agregar un servicio
  //     Route::post('/operador/servicio/{idregistrarlaboratorio}', [RegistroLaboratorioController::class, 'ingresarServicio']);
  //     // Editar un servicio
  //     Route::put('/operador/servicio/editar/{idregistrarlaboratorio}', [RegistroLaboratorioController::class, 'modificarServicio']);
  //     // Eliminar un servicio
  //     Route::put('/operador/servicio/eliminar/{idregistrarlaboratorio}', [RegistroLaboratorioController::class, 'eliminarServicio']);

  //     //RUTAS PARA PUBLICACIONES
  //     // Listar publicaciones
  //     Route::get('/operador/publicaciones/{idregistrarlaboratorio}', [PublicacionController::class, 'index']);
  //     // Mostrar una publicacion
  //     Route::get('/operador/publicacion/show/{idpublicacion}', [PublicacionController::class, 'show']);
  //     // Agregar una nueva publicacion
  //     Route::post('/operador/publicacion', [PublicacionController::class, 'store']);
  //     // Editar una publicacion
  //     Route::put('/operador/publicacion/{idpublicacion}', [PublicacionController::class, 'update']);
  //     // Eliminar una publicacion
  //     Route::delete('/operador/publicacion/{idpublicacion}', [PublicacionController::class, 'delete']);

  //     // RUTAS PARA GALERIA DE FOTOS DE LABORATORIO
  //     // Listar la galeria de fotos de un laboratorio
  //     Route::get('/operador/galeriaLaboratorios/{idlaboratorio}', [GaleriaLaboratorioController::class, 'index']);
  //     // Registrar un una nueva imagen de un laboratorio
  //     Route::post('/operador/galeriaLaboratorio', [GaleriaLaboratorioController::class, 'store']);
  //     // Mostrar un registro de galeria laboratorio
  //     Route::get('/operador/galeriaLaboratorio/{idgaleria}', [GaleriaLaboratorioController::class, 'show']);
  //     // Editar un registro de galeria laboratorio
  //     Route::post('/operador/galeriaLaboratorio/{idgaleria}', [GaleriaLaboratorioController::class, 'update']);
  //     // Elimina un registro de galeria laboratorio
  //     Route::delete('/operador/galeriaLaboratorio/{idgaleria}', [GaleriaLaboratorioController::class, 'destroy']);
  // });

  // Route::middleware(['auth:sanctum', 'admininstitutos'])->group(function () {
  //     //RUTAS INSTITUTOS
  //     Route::get('/institutos/director/{usuario_director}', [InstitutoController::class, 'indexByDirector']);
  //     //Registrar instituto
  //     Route::post('/institutos', [InstitutoController::class, 'store']);
  //     //Mostrar instituto por id
  //     Route::get('/institutos/show/{idinstituto}', [InstitutoController::class, 'show']);
  //     //Actualizar instituto
  //     Route::put('/institutos/{idinstituto}', [InstitutoController::class, 'update']);
  //     //Eliminar instituto por id
  //     Route::delete('/institutos/{idinstituto}', [InstitutoController::class, 'delete']);

  //     //DIRECTORES
  //     //Lista todos los directores
  //     Route::get('/directores', [UserController::class, 'listarDirectores']);
  //     // //COmpletar la imfor de los
  //     // Route::post('/directores/completar/{iddirector}', [UserController::class, 'completarInfoDirector']);
  //     // Lista de institutos asignados al director por Id
  //     Route::get('/directores/{usuario_director}', [InstitutoController::class, 'indexByDirector']);
  //     //Completar info de un instituto
  //     Route::post('/directores/completar/{idinstituto}', [InstitutoController::class, 'completarInstituto']);
  //     // Listar instituto de director
  //     Route::get('/institutosPorDirector/{usuario_director}', [InstitutoController::class, 'VistaInstitutoDirector']);

  //     // RUTAS DIRECTOR ---------------------------------------------------------------------------------------
  //     Route::get('/comiteDirectivo', [UserController::class, 'indexComiteDirectivo']);
  //     Route::get('/comiteDirectivo/{idComteDirectivo}', [UserController::class, 'showComiteDirectivo']);
  //     Route::post('/comiteDirectivo', [UserController::class, 'storeComiteDirectivo']);
  //     Route::put('/comiteDirectivo/{idComteDirectivo}', [UserController::class, 'updateComiteDirectivo']);
  //     Route::delete('/comiteDirectivo/{idComteDirectivo}', [UserController::class, 'deleteComiteDiretivo']);


  //     // RUTA PARA LOS PROYECTOS DE INSTITUTO
  //     // Lista todos los proyectos de un instituto
  //     Route::get('/proyectos/instituto/{idinstituto}', [ProyectoInstitutoController::class, 'index']);
  //     // Crea un proyecto de un instituto
  //     Route::post('/proyecto/instituto', [ProyectoInstitutoController::class, 'store']);
  //     // Muestra un proyecto de un instituto
  //     Route::get('/proyecto/instituto/{idproyecto}', [ProyectoInstitutoController::class, 'show']);
  //     // Actualiza un proyecto de un instituto
  //     Route::post('/proyecto/instituto/update/{idproyecto}', [ProyectoInstitutoController::class, 'update']);
  //     // Elimina un proyecto de un instituto
  //     Route::delete('/proyecto/instituto/{idproyecto}', [ProyectoInstitutoController::class, 'destroy']);


  //     //RUTA PARA FUNCIONES DE UN INSTITUTO
  //     //Lista funciones por id de instituto
  //     Route::get('/directores/funciones/{id_instituto}', [FuncionesInstitutoController::class, 'index']);
  //     //Registra funciones
  //     Route::post('/directores/funciones', [FuncionesInstitutoController::class, 'store']);
  //     //Busca una sola funcion
  //     Route::get('/directores/funciones/show/{idfuncion}', [FuncionesInstitutoController::class, 'show']);
  //     //Actualiza funcion
  //     Route::post('/directores/funciones/{idfuncion}', [FuncionesInstitutoController::class, 'update']);
  //     //Elimina funcion
  //     Route::delete('/directores/funciones/eliminar/{idfuncion}', [FuncionesInstitutoController::class, 'delete']);

  //     // RUTA PARA PUBLICACIONES DE UN INSTITUTO
  //     // Listar las publicaciones de un instituto
  //     Route::get('/publicacion/instituto/{idinstituto}', [PublicacionInstitutoController::class, 'index']);
  //     // Craer una nueva publicacion de instituto
  //     Route::post('/publicacion/instituto', [PublicacionInstitutoController::class, 'store']);
  //     // Mostrar una publicacion de instituto
  //     Route::get('/publicacion/instituto/show/{idpublicacion}', [PublicacionInstitutoController::class, 'show']);
  //     // Editar una publicacion de instituto
  //     Route::put('/publicacion/instituto/{idpublicacion}', [PublicacionInstitutoController::class, 'update']);
  //     // Eliminar una publicacion de instituto
  //     Route::delete('/publicacion/instituto/{idpublicacion}', [PublicacionInstitutoController::class, 'destroy']);

  //     // RUTA PARA CONVENIOS DE UN INSTITUTO
  //     //Mostrar convenios por id instituto
  //     Route::get('/directores/convenios/{idinstituto}', [ConvenioIntitutoController::class, 'index']);
  //     //Registra convenios
  //     Route::post('/directores/convenios', [ConvenioIntitutoController::class, 'store']);
  //     //Busca una sola convenios
  //     Route::get('/directores/convenios/show/{idfuncion}', [ConvenioIntitutoController::class, 'show']);
  //     //Actualiza convenios
  //     Route::put('/directores/convenios/{idfuncion}', [ConvenioIntitutoController::class, 'update']);
  //     //Elimina convenios
  //     Route::delete('/directores/convenios/eliminar/{idfuncion}', [ConvenioIntitutoController::class, 'delete']);
  // });

});




//RUTAS PARA USUARIOS INVITADOS
//Muestra todos los intitutos
Route::get('/institutos', [InstitutoController::class, 'index']);

//Muestra todos los laboratorios
Route::get('/registroLaboratorioPublico', [RegistroLaboratorioController::class, 'VisLabCoordinadorpublic']);
// Route::put('/operador/proyectos/{idproyecto}', [ProyectoController::class, 'delete']);
Route::get('/proyectos/imagen/{idproyecto}', [ProyectoController::class, 'getImagen']);
// Obtener laboratorio por id con objeto coordinador interno
Route::get('/laboratorioCoordinador/{idLaboratorio}', [RegistroLaboratorioController::class, 'getLaboratorioByIdLaboratorioPublic']);

// Listar los equipos
Route::get('/invitado/equipos/{idregistrarlaboratio}', [EquipoController::class, 'index']);
//Mostrar proyecto por   id
Route::get('/invitado/equipos/show/{idequipo}', [EquipoController::class, 'show']);

//Mostrar mostrar proyectos
Route::get('/invitado/proyectos/{idregistrarlaboratio}', [ProyectoController::class, 'index']);
//Mostrar proyecto por id
Route::get('/invitado/proyectos/show/{idproyecto}', [ProyectoController::class, 'show']);

// Listar disciplinas o lineas de investigacion
Route::get('/invitado/disciplinas', [DisciplinaController::class, 'index']);

// Listar los servicios de un laboratorio
Route::get('/invitado/servicios/{idregistrarlaboratorio}', [RegistroLaboratorioController::class, 'listarServicios']);

// Listar las publicaciones de un laboratorio
Route::get('/invitado/publicaciones/{idregistrarlaboratorio}', [PublicacionController::class, 'index']);
// Route::get('/solicitudes', [SolicitudController::class, 'index']);
// Route::get('/solicitud/{idsolicitud}', [SolicitudController::class, 'show']);

// Listar la galeria de fotos de un laboratorio
Route::get('/invitado/galeriaLaboratorios/{idlaboratorio}', [GaleriaLaboratorioController::class, 'index']);
