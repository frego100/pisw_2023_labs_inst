<?php

namespace Tests\Feature;

use App\Models\Area;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;

class AreaTest extends TestCase
{
    use RefreshDatabase; // Para reiniciar la base de datos antes de cada prueba

    public function testIndex()
    {
        // Deshabilitar todos los middleware de autenticación
        $this->withoutMiddleware();
        // Insertar datos de prueba en la base de datos
        Area::factory()->create(['nombre' => 'Área 1', 'estado' => true]);
        Area::factory()->create(['nombre' => 'Área 2', 'estado' => true]);

        $response = $this->get('/api/areas');

        $response->assertStatus(200)
            ->assertJsonStructure(['areas']);
    }

    public function testStore()
    {
         // Deshabilitar todos los middleware de autenticación
        $this->withoutMiddleware();

        $data = ['nombre' => 'Nueva Área'];

        $response = $this->post('/api/areas', $data);

        $response->assertStatus(200)
            ->assertJson(['status' => 'Creado Correctamente']);
    }

    public function testShow()
    {
         // Deshabilitar todos los middleware de autenticación
        $this->withoutMiddleware();
        
        $area = Area::factory()->create(['nombre' => 'Área de prueba', 'estado' => true]);

        $response = $this->get("/api/areas/{$area->area_id}");

        $response->assertStatus(200)
            ->assertJson(['area' => ['nombre' => 'Área de prueba']]);
    }

    public function testUpdate()
    {

         // Deshabilitar todos los middleware de autenticación
        $this->withoutMiddleware();

        $area = Area::factory()->create(['nombre' => 'Área de prueba', 'estado' => true]);
        $data = ['nombre' => 'Área actualizada'];

        $response = $this->put("/api/areas/{$area->area_id}", $data);

        $response->assertStatus(200)
            ->assertJson(['status' => 'Actualizado Correctamente']);
    }

    public function testDelete()
    {
         // Deshabilitar todos los middleware de autenticación
        $this->withoutMiddleware();
        
        $area = Area::factory()->create(['nombre' => 'Área a eliminar', 'estado' => true]);

        $response = $this->delete("/api/areas/{$area->area_id}");

        $response->assertStatus(200)
            ->assertJson(['status' => 'Eliminado Correctamente']);
    }
}
