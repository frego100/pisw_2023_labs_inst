<?php

namespace Tests\Feature;

use App\Models\Disciplina;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class DisciplinaTest extends TestCase
{
    use RefreshDatabase; // Para reiniciar la base de datos antes de cada prueba
    
    public function testIndex()
    {
        // Deshabilitar todos los middleware de autenticación
        $this->withoutMiddleware();
        // Insertar datos de prueba en la base de datos
        Disciplina::factory()->create(['nombre' => 'Disciplina 1', 'estado' => true]);
        Disciplina::factory()->create(['nombre' => 'Disciplina 2', 'estado' => true]);

        $response = $this->get('/api/disciplinas');

        $response->assertStatus(200)
            ->assertJsonStructure(['disciplinas']);
    }

    public function testStore()
    {
         // Deshabilitar todos los middleware de autenticación
        $this->withoutMiddleware();

        $data = ['nombre' => 'Nueva Disciplina'];

        $response = $this->post('/api/disciplinas', $data);

        $response->assertStatus(200)
            ->assertJson(['status' => 'Creado Correctamente']);
    }

    public function testShow()
    {
         // Deshabilitar todos los middleware de autenticación
        $this->withoutMiddleware();
        
        $disciplina = Disciplina::factory()->create(['nombre' => 'Disciplina de prueba', 'estado' => true]);

        $response = $this->get("/api/disciplinas/{$disciplina->disciplina_id}");

        $response->assertStatus(200)
            ->assertJson(['disciplina' => ['nombre' => 'Disciplina de prueba']]);
    }

    public function testUpdate()
    {

         // Deshabilitar todos los middleware de autenticación
        $this->withoutMiddleware();

        $disciplina = Disciplina::factory()->create(['nombre' => 'Disciplina de prueba 2', 'estado' => true]);
        $data = ['nombre' => 'Disciplina actualizada'];

        $response = $this->put("/api/disciplinas/{$disciplina->disciplina_id}", $data);

        $response->assertStatus(200)
            ->assertJson(['status' => 'Actualizado Correctamente']);
    }

    public function testDelete()
    {
         // Deshabilitar todos los middleware de autenticación
        $this->withoutMiddleware();
        
        $disciplina = Disciplina::factory()->create(['nombre' => 'Disciplina a eliminar', 'estado' => true]);

        $response = $this->delete("/api/disciplinas/{$disciplina->disciplina_id}");

        $response->assertStatus(200)
            ->assertJson(['status' => 'Eliminado Correctamente']);
    }
}
